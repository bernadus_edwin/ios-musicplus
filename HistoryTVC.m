//
//  HistoryTVC.m
//  musicplus
//
//  Created by Erlangga on 11/3/13.
//  Copyright (c) 2013 Edward Julianto. All rights reserved.
//

#import "HistoryTVC.h"

@interface HistoryTVC ()
{
    NSMutableArray * mediaMutableArray;
}
@end

@implementation HistoryTVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        [self setTitle:@"Download History"];
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"History" image:[UIImage imageNamed:@"iconhistory.png"] tag:8];
        

    }
    return self;
}

-(void) refreshDataFromUI
{
    [self refreshData];
    if ([self.refreshControl isRefreshing]) [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}
-(void) refreshData
{
    //NSLog(@"refreshData");
    //[mediaMutableArray release];
    mediaMutableArray = nil;
    mediaMutableArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"histDownload"]];
    //NSLog(@"refresh data %@" , mediaMutableArray);
    
    
}

-(void) viewWillAppear:(BOOL)animated
{
    //NSLog(@"willAppear");
    [self refreshData];
    [self.tableView reloadData];
    [super viewWillAppear:animated];
    
}
- (void)viewDidLoad
{
    //NSLog(@"didload");
    [super viewDidLoad];
    NSData * greendata = [[NSUserDefaults standardUserDefaults] objectForKey:@"TintColor"];
    UIColor *greenish = [NSKeyedUnarchiver unarchiveObjectWithData:greendata];
    
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl setHidden:NO];
    [self.refreshControl addTarget:self action:@selector(refreshDataFromUI) forControlEvents:UIControlEventValueChanged];
    [self.refreshControl setAttributedTitle:[[NSAttributedString alloc] initWithString:@"pull down to refresh"]];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"clear all" style:UIBarButtonItemStylePlain target:self action:@selector(clearHist)];
    [self.navigationItem.rightBarButtonItem setTintColor:greenish];
    [self.navigationItem.backBarButtonItem setTintColor:greenish];
    [self.refreshControl setTintColor:greenish];
}
-(void) clearHist
{
    NSArray * array = [[NSArray alloc] init];
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"histDownload"];
    [self refreshData];
    [self.tableView reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return mediaMutableArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MediaCell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
    // Configure the cell...
    [cell.textLabel setText:[[mediaMutableArray objectAtIndex:indexPath.row] valueForKey:@"filename"]];
    [cell.detailTextLabel setText:[[mediaMutableArray objectAtIndex:indexPath.row] valueForKey:@"url"]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *current = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    UIPasteboard * pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = current.detailTextLabel.text;
    UIAlertView * alert  = [[UIAlertView alloc] initWithTitle:@"Clipboard" message:@"URL Download coppied to clipboard\n you can paste it on browser" delegate:nil cancelButtonTitle:@"close" otherButtonTitles: nil];
    [alert show];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
