//
//  DownloadSession.m
//  musicplus
//
//  Created by Erlangga on 12/7/13.
//  Copyright (c) 2013 Edward Julianto. All rights reserved.
//
//
//  Download.m
//  TestingPlatform
//
//  Created by Robert Ryan on 11/13/12.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "DownloadSession.h"
#import "AppDelegate.h"
//@interface DownloadSession () <NSURLConnectionDelegate>
@interface DownloadSession () <NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDownloadDelegate, NSURLSessionDataDelegate>
@property (strong, nonatomic) NSOutputStream *downloadStream;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSString *tempFilename;

@property (nonatomic, retain) NSURLSession *session;
@property (nonatomic, retain) NSURLSessionDownloadTask *downloadTask;
@property (nonatomic, assign) AFURLSessionManager *sessionManagerRef;

@end

@implementation DownloadSession
@synthesize filename, url, delegate, tempFilename, errorDownload, connection, downloading, downloadStream, expectedContentLength, progressContentLength, sessionManager, progress;

#pragma mark - Public methods

- (id)initWithFilename:(NSString *)filename2 URL:(NSURL *)url2 delegate:(id<DownloadSessionDelegate>)delegate2
{
    self = [super init];
    
    if (self)
    {
        //_filename = filename;
        self.filename = filename2;
        self.url = url2;
        self.delegate = delegate2;
    }
    
    return self;
}

- (void)start
{
    // initialize progress variables
    
    self.downloading = YES;
    self.expectedContentLength = -1;
    self.progressContentLength = 0;
    
    
    NSURLRequest * req = [[NSURLRequest alloc] initWithURL:self.url];
    [self.sessionManager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        expectedContentLength = totalBytesExpectedToWrite;
        
        [self.delegate downloadDidReceiveData:self];
        
    }];

    self.downloadTask = [ sessionManager
                         downloadTaskWithRequest:req
                         progress:&progress
                         destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
                             NSLog(@"Destination %@", targetPath);
                             NSLog(@"Destination response %@", response);
                             self.filename = [self getAutoRenameFilePath:self.filename];
                             return [[NSURL alloc] initFileURLWithPath:self.filename];
                         }
                         completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
                             [progress retain];
                             NSLog(@"Completion handler response %@", response);
                             NSLog(@"Completion handler response %@", filePath);
                             NSLog(@"Completion handler response %@", error);
                             
                             
                             if (error == nil)
                             {
                                 [self cleanupConnectionSuccessful:YES];
                                 
                             }
                             else
                             {
                                 [self cleanupConnectionSuccessful:NO];
                             }
                             
                         }
                         ];
    [self.downloadTask resume];
    [progress retain];
    
}

- (void)cancel
{
    [self cleanupConnectionSuccessful:NO];
}

#pragma mark - Private methods

- (BOOL)createFolderForPath:(NSString *)filePath
{
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *folder = [filePath stringByDeletingLastPathComponent];
    BOOL isDirectory;
    
    if (![fileManager fileExistsAtPath:folder isDirectory:&isDirectory])
    {
        // if folder doesn't exist, try to create it
        
        [fileManager createDirectoryAtPath:folder withIntermediateDirectories:YES attributes:nil error:&error];
        
        // if fail, report error
        
        if (error)
        {
            self.errorDownload = error;
            return FALSE;
        }
        
        // directory successfully created
        
        return TRUE;
    }
    else if (!isDirectory)
    {
        self.errorDownload = [NSError errorWithDomain:[NSBundle mainBundle].bundleIdentifier
                                                 code:-1
                                             userInfo:@{@"message": @"Unable to create directory; file exists by that name", @"function" : @(__FUNCTION__), @"folder": folder}];
        return FALSE;
    }
    
    // directory already existed
    
    return TRUE;
}
-(NSString *) getAutoRenameFilePath:(NSString *) filename2
{
    if([[NSFileManager defaultManager] fileExistsAtPath:filename2])
    {
        NSInteger index= 1;
        NSString * newfilename =  [NSString stringWithFormat:@"%@-%d.%@", [filename2  stringByDeletingPathExtension] , index, [filename2 pathExtension]];
    
    //BOOL blnRes = [[NSFileManager defaultManager] fileExistsAtPath:newfilename];
        while ([[NSFileManager defaultManager] fileExistsAtPath:newfilename])
        {
            newfilename = [NSString stringWithFormat:@"%@-%d.%@", [filename2 stringByDeletingPathExtension] , index+=1, [filename pathExtension]];
        }
        return newfilename;
    }
    else
        return filename2;
    
}
-(NSString *) getNewFileIncremented:(NSString* ) filename3 Dest:(NSString *) documentDirectory
{
    //add "_#" to file name
    //for # = 1 sd unlimited, rewrite #
    //try save _#
    NSInteger index = 1;
    NSString * newfilename =  [NSString stringWithFormat:@"%@-%d.%@", [filename stringByDeletingPathExtension], index, [filename3 pathExtension]];
    NSString * destpath = [documentDirectory stringByAppendingPathComponent:newfilename];
    while ([[NSFileManager defaultManager] fileExistsAtPath:destpath]) {
        newfilename = [NSString stringWithFormat:@"%@-%d.%@", [filename stringByDeletingPathExtension], index++, [filename pathExtension]];
        destpath = [documentDirectory stringByAppendingPathComponent:newfilename];
    }
    return newfilename;
}
- (void)cleanupConnectionSuccessful:(BOOL)success
{
    
    self.downloading = NO;
    if (success)
    {
        [delegate downloadDidFinishLoading:self];
    }
    else
    {
        [delegate downloadDidFail:self];
    }

    
    
}

- (NSString *)pathForTemporaryFileWithPrefix:(NSString *)prefix
{
    NSString *  result;
    CFUUIDRef   uuid;
    CFStringRef uuidStr;
    
    uuid = CFUUIDCreate(NULL);
    assert(uuid != NULL);
    
    uuidStr = CFUUIDCreateString(NULL, uuid);
    assert(uuidStr != NULL);
    
    result = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-%@", prefix, uuidStr]];
    assert(result != nil);
    
    CFRelease(uuidStr);
    CFRelease(uuid);
    
    return result;
}
#pragma mark - NSURLSessionDataDelegate methods

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler
{
    if ([response isKindOfClass:[NSHTTPURLResponse class]])
    {
        NSHTTPURLResponse *httpResponse = (id)response;
        
        NSInteger statusCode = [httpResponse statusCode];
        
        if (statusCode == 200)
        {
            self.expectedContentLength = [response expectedContentLength];
        }
        else if (statusCode >= 400)
        {
            self.errorDownload = [NSError errorWithDomain:[NSBundle mainBundle].bundleIdentifier
                                                     code:statusCode
                                                 userInfo:@{
                                                            @"message" : @"bad HTTP response status code",
                                                            @"function": @(__FUNCTION__),
                                                            @"NSHTTPURLResponse" : response
                                                            }];
            [self cleanupConnectionSuccessful:NO];
        }
    }
    else
    {
        self.expectedContentLength = -1;
    }

}

/* Notification that a data task has become a download task.  No
 * future messages will be sent to the data task.
 */
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
didBecomeDownloadTask:(NSURLSessionDownloadTask *)downloadTask
{
    
}

/* Sent when data is available for the delegate to consume.  It is
 * assumed that the delegate will retain and not copy the data.  As
 * the data may be discontiguous, you should use
 * [NSData enumerateByteRangesUsingBlock:] to access it.
 */
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
    NSInteger       dataLength = [data length];
    const uint8_t * dataBytes  = [data bytes];
    NSInteger       bytesWritten;
    NSInteger       bytesWrittenSoFar;
    
    bytesWrittenSoFar = 0;
    do {
        bytesWritten = [self.downloadStream write:&dataBytes[bytesWrittenSoFar] maxLength:dataLength - bytesWrittenSoFar];
        assert(bytesWritten != 0);
        if (bytesWritten == -1) {
            [self cleanupConnectionSuccessful:NO];
            break;
        } else {
            bytesWrittenSoFar += bytesWritten;
        }
    } while (bytesWrittenSoFar != dataLength);
    
    self.progressContentLength += dataLength;
    
    if ([self.delegate respondsToSelector:@selector(downloadDidReceiveData:)])
        [self.delegate downloadDidReceiveData:self];
}

/* Invoke the completion routine with a valid NSCachedURLResponse to
 * allow the resulting data to be cached, or pass nil to prevent
 * caching. Note that there is no guarantee that caching will be
 * attempted for a given resource, and you should not rely on this
 * message to receive the resource data.
 */
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
 willCacheResponse:(NSCachedURLResponse *)proposedResponse
 completionHandler:(void (^)(NSCachedURLResponse *cachedResponse))completionHandler
{
    
}



/*
#pragma mark - NSURLConnectionDataDelegate methods

- (void)connection:(NSURLConnection*)connection didReceiveResponse:(NSURLResponse *)response
{
    if ([response isKindOfClass:[NSHTTPURLResponse class]])
    {
        NSHTTPURLResponse *httpResponse = (id)response;
        
        NSInteger statusCode = [httpResponse statusCode];
        
        if (statusCode == 200)
        {
            self.expectedContentLength = [response expectedContentLength];
        }
        else if (statusCode >= 400)
        {
            self.errorDownload = [NSError errorWithDomain:[NSBundle mainBundle].bundleIdentifier
                                                     code:statusCode
                                                 userInfo:@{
                                                            @"message" : @"bad HTTP response status code",
                                                            @"function": @(__FUNCTION__),
                                                            @"NSHTTPURLResponse" : response
                                                            }];
            [self cleanupConnectionSuccessful:NO];
        }
    }
    else
    {
        self.expectedContentLength = -1;
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSInteger       dataLength = [data length];
    const uint8_t * dataBytes  = [data bytes];
    NSInteger       bytesWritten;
    NSInteger       bytesWrittenSoFar;
    
    bytesWrittenSoFar = 0;
    do {
        bytesWritten = [self.downloadStream write:&dataBytes[bytesWrittenSoFar] maxLength:dataLength - bytesWrittenSoFar];
        assert(bytesWritten != 0);
        if (bytesWritten == -1) {
            [self cleanupConnectionSuccessful:NO];
            break;
        } else {
            bytesWrittenSoFar += bytesWritten;
        }
    } while (bytesWrittenSoFar != dataLength);
    
    self.progressContentLength += dataLength;
    
    if ([self.delegate respondsToSelector:@selector(downloadDidReceiveData:)])
        [self.delegate downloadDidReceiveData:self];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [self cleanupConnectionSuccessful:YES];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    self.errorDownload = error;
    NSLog(@"Error Download %@", error);;
    [self cleanupConnectionSuccessful:NO];
}
 */
#pragma mark - Background downloader 
- (NSURLSession *)backgroundSession
{
    /*
     Using disptach_once here ensures that multiple background sessions with the same identifier are not created in this instance of the application. If you want to support multiple background sessions within a single process, you should create each session with its own identifier.
     */
	static NSURLSession *session = nil;
    dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration backgroundSessionConfiguration:[NSString stringWithFormat:@"com.musicplus.%@", [self.filename lastPathComponent]] ];
        
		session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue: nil];
	});

	return session;
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    if (downloadTask == self.downloadTask)
    {
        NSLog(@"ER DEBUG >>>>>>>> Session Download Task did finish ");
        //self.tempFilename = location.absoluteString;
        self.tempFilename = [location path];
        
        [self cleanupConnectionSuccessful:YES];
        
        
        
    }
}
-(void) URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    if (downloadTask == self.downloadTask)
    {
    //NSLog(@"ER DEBUG >>>>>>>> %lld, %lld, %lld, %@",
     //    totalBytesExpectedToWrite,  self.progressContentLength, downloadTask.countOfBytesReceived,     session.configuration.identifier);

    /*NSInteger       dataLength = [data length];
    const uint8_t * dataBytes  = [data bytes];
    NSInteger       bytesWritten;
    NSInteger       bytesWrittenSoFar;
    
    bytesWrittenSoFar = 0;
    do {
        bytesWritten = [self.downloadStream write:&dataBytes[bytesWrittenSoFar] maxLength:dataLength - bytesWrittenSoFar];
        assert(bytesWritten != 0);
        if (bytesWritten == -1) {
            [self cleanupConnectionSuccessful:NO];
            break;
        } else {
            bytesWrittenSoFar += bytesWritten;
        }
    } while (bytesWrittenSoFar != dataLength);
    */
    self.expectedContentLength = totalBytesExpectedToWrite;
    if ( totalBytesWritten -  self.progressContentLength > 1 )
    {   self.progressContentLength = totalBytesWritten;
        if ([self.delegate respondsToSelector:@selector(downloadDidReceiveData:)])
            dispatch_async(dispatch_get_main_queue(), ^{
               [self.delegate downloadDidReceiveData:self];
            });
    }
    }
}
-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes
{
    NSLog(@"ER DEBUG >>>>>>>> Session Download Task did resume ");
}
/*
 If an application has received an -application:handleEventsForBackgroundURLSession:completionHandler: message, the session delegate will receive this message to indicate that all messages previously enqueued for this session have been delivered. At this time it is safe to invoke the previously stored completion handler, or to begin any internal updates that will result in invoking the completion handler.
 */
- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.backgroundSessionCompletionHandler) {
        void (^completionHandler)() = appDelegate.backgroundSessionCompletionHandler;
        appDelegate.backgroundSessionCompletionHandler = nil;
        completionHandler();
    }
    
    NSLog(@"All tasks are finished");
}
@end

