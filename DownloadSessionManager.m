//
//  DownloadSessionManager.m
//  musicplus
//
//  Created by Erlangga on 12/7/13.
//  Copyright (c) 2013 Edward Julianto. All rights reserved.
//
//
//  DownloadSessionManager.m
//  TestingPlatform
//
//  Created by Robert Ryan on 11/21/12.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "DownloadSessionManager.h"
#import "DownloadSession.h"
#import "AFURLSessionManager.h"

@interface DownloadSessionManager () <DownloadSessionDelegate, NSURLSessionDelegate>

@property (nonatomic) BOOL cancelAllInProgress;

@end

@implementation DownloadSessionManager

- (id)init
{
    self = [super init];
    
    if (self)
    {
        
        _downloads = [[NSMutableArray alloc] init];
        _maxConcurrentDownloads = 3;
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration backgroundSessionConfiguration:@"com.musicplus.download"];
        self.sessionManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    }
    
    return self;
}

#pragma mark - DownloadSessionManager public methods

- (void)addDownloadWithFilename:(NSString *)filename URL:(NSURL *)url
{
    DownloadSession *download = [[DownloadSession alloc] initWithFilename:filename URL:url delegate:self];
    [download setSessionManager:self.sessionManager];
    [self.downloads addObject:download];
}

- (void)start
{
    [self tryDownloading];
}

- (void)cancelAll
{
    self.cancelAllInProgress = YES;
    
    while ([self.downloads count] > 0)
    {
        [[self.downloads objectAtIndex:0] cancel];
    }
    
    self.cancelAllInProgress = NO;
    
    [self informDelegateThatDownloadsAreDone];
}

- (id)initWithDelegate:(id<DownloadSessionManagerDelegate>)delegate
{
    self = [self init];
    
    if (self)
    {
        _delegate = delegate;
        
    }
    
    return self;
}

#pragma mark - DownloadDelegate Methods

- (void)downloadDidFinishLoading:(DownloadSession *)download
{
    [self.downloads removeObject:download];
    
    if ([self.delegate respondsToSelector:@selector(DownloadSessionManager:downloadDidFinishLoading:)])
    {
        [self.delegate DownloadSessionManager:self downloadDidFinishLoading:download];
    }
    
    [self tryDownloading];
}

- (void)downloadDidFail:(DownloadSession *)download
{
    [self.downloads removeObject:download];
    
    if ([self.delegate respondsToSelector:@selector(DownloadSessionManager:downloadDidFail:)])
        [self.delegate DownloadSessionManager:self downloadDidFail:download];
    
    if (!self.cancelAllInProgress)
    {
        [self tryDownloading];
    }
}

- (void)downloadDidReceiveData:(DownloadSession *)download
{
    if ([self.delegate respondsToSelector:@selector(DownloadSessionManager:downloadDidReceiveData:)])
    {
        [self.delegate DownloadSessionManager:self downloadDidReceiveData:download];
    }
}

#pragma mark - Private methods

- (void)informDelegateThatDownloadsAreDone
{
    if ([self.delegate respondsToSelector:@selector(didFinishLoadingAllForManager:)])
    {
        [self.delegate didFinishLoadingAllForManager:self];
    }
}

- (void)tryDownloading
{
    NSInteger totalDownloads = [self.downloads count];
    
    // if we're done, inform the delegate
    self.maxConcurrentDownloads = 1;
    if (totalDownloads == 0)
    {
        [self informDelegateThatDownloadsAreDone];
        return;
    }
    
    // while there are downloads waiting to be started and we haven't hit the maxConcurrentDownloads, then start
    NSLog(@"///////////////////////////////////");
    NSLog(@"DEBUG:current download = %d", totalDownloads);
    while ([self countUnstartedDownloads] > 0 && [self countActiveDownloads] < self.maxConcurrentDownloads)
    {
        NSLog(@"DEBUG:unstarted %d", [self countUnstartedDownloads]);
        NSLog(@"DEBUG:Activated %d", [self countActiveDownloads]);

        for (DownloadSession *download in self.downloads)
        {
            NSLog(@"DEBUG:isDownloading %d", download.isDownloading);
            
            if (!download.isDownloading)
            {
                [download start];
                break;
            }
        }
    }
    NSLog(@"///////////////////////////////////");
    NSLog(@" ");
}

- (NSInteger)countUnstartedDownloads
{
    return [self.downloads count] - [self countActiveDownloads];
}

- (NSInteger)countActiveDownloads
{
    NSInteger activeDownloadCount = 0;
    
    for (DownloadSession *download in self.downloads)
    {
        if (download.isDownloading)
            activeDownloadCount++;
    }
    
    return activeDownloadCount;
}
 
@end

