//
//  myWebViewClass.m
//  musicplus
//
//  Created by Erlangga on 10/23/13.
//  Copyright (c) 2013 Edward Julianto. All rights reserved.
//

#import "myWebViewClass.h"

@implementation myWebViewClass

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(BOOL)canGoBack
{
    return YES;
}
-(BOOL)canGoForward
{
    return YES;
}
-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    return YES;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
