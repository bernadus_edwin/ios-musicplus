//
//  BookmarkTVC.h
//  Music+
//
//  Created by EIT-MACMINI02 on 9/22/13.
//  Copyright (c) 2013 Trinity. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BrowserVC;
@interface BookmarkTVC : UITableViewController
{
    NSString * stringURL;
    NSMutableArray * arrayBookmarks;
}
@property (nonatomic, retain) NSMutableArray * arrayBookmarks;
@property (nonatomic, retain) NSString * stringURL;
@property (nonatomic, retain) BrowserVC * parent;


-(void) addToBookmark;


@end
