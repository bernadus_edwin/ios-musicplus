//
//  BrowserVC.h
//  Music+
//
//  Created by EIT-MACMINI02 on 9/20/13.
//  Copyright (c) 2013 Trinity. All rights reserved.
//

#import <UIKit/UIKit.h> 

#import "AutoDownloadVC.h"
@interface BrowserVC : UIViewController <UIWebViewDelegate, UITextFieldDelegate, UIActionSheetDelegate, NSURLConnectionDataDelegate>
{
    long expectedProgress;
    NSString * prevString;
    NSString * downloadString;
    NSMutableData *tempData;
    NSMutableArray *toolbarArray;
    IBOutlet UIToolbar * toolbar;
    IBOutlet UIProgressView * progressBar;
    AutoDownloadVC *advc;
    
   NSString * urlString ;
}
@property (nonatomic) long expectedProgress;
@property (nonatomic, strong) NSString * urlString ;
@property(nonatomic, retain)NSMutableArray * toolbarArray;
@property(nonatomic, retain) IBOutlet UIProgressView * progressBar;
@property(nonatomic, retain) IBOutlet UIToolbar * toolbar;
@property(nonatomic, retain) IBOutlet UIWebView * myWebView;
@property(nonatomic, retain) IBOutlet UIBarButtonItem  * goButton;
@property(nonatomic, retain) IBOutlet UIBarButtonItem   * homeButton;
@property(nonatomic, retain) IBOutlet UITextField * urlField;
//@property(nonatomic, retain) IBOutlet UIBarButtonItem * btn1;
@property(nonatomic, retain) IBOutlet UIBarButtonItem * backButton;
@property(nonatomic, retain) IBOutlet UIBarButtonItem * forwardButton;
@property(nonatomic, retain) IBOutlet UIBarButtonItem * bookmarkButton;
@property(nonatomic, retain) IBOutlet UIBarButtonItem * reloadButton;
@property(nonatomic, retain) IBOutlet UIBarButtonItem * stopButton;
@property(nonatomic, retain) IBOutlet UIBarButtonItem * sheetButton;
@property(nonatomic, retain) IBOutlet UIBarButtonItem * hideButton;
@property(nonatomic, retain) IBOutlet UIBarButtonItem * showButton;
@property(nonatomic, retain) IBOutlet UIActivityIndicatorView * loadingView;
@property(nonatomic, retain) IBOutlet UIActionSheet * actionSheet;

-(IBAction)showActionSheet:(id)sender;
-(IBAction)showAutoDownload:(id)sender;
-(IBAction)showBookmarkVC:(id) sender;
-(IBAction)loadwebrequest;
-(IBAction) myBack:(id)sender;
-(IBAction) myForward:(id)sender;
-(IBAction) myStop:(id)sender;
-(IBAction) myRefresh:(id)sender;
-(IBAction) myHome:(id)sender;
-(IBAction) myBookmark:(id)sender;
-(IBAction) toogleTabbar:(id)sender;
-(IBAction) myAutoDownload:(id)sender;
-(void) shouldLoadWebRequest:(NSString *) val;

@end
