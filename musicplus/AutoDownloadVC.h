//
//  AutoDownloadVC.h
//  Music+
//
//  Created by EIT-MACMINI02 on 9/20/13.
//  Copyright (c) 2013 Trinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutoDownloadVC : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate, UIAlertViewDelegate>
{
    NSString *urlString;
    NSInteger count;
    NSString *renamedFile;
}
@property (nonatomic, retain) NSString * renamedFile; 
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, strong) NSString * urlString ;
@property (nonatomic, retain) IBOutlet UIPickerView *myPickerView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem * doneButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem * cancelButton;

-(IBAction)actionButton : (id) sender;

@end
