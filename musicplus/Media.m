//
//  Media.m
//  musicplus
//
//  Created by Erlangga on 10/9/13.
//  Copyright (c) 2013 Edward Julianto. All rights reserved.
//

#import "Media.h"
#import "DownloadObject.h"
#import "HistoryTVC.h"
#import "DownloadCell.h"
#import "AppDelegate.h"
@implementation Media
@synthesize mediaMutableArray, dManager, currentIndex, aManager;




////////////////
- (void)awakeFromNib
{ 
    dManager = [[DownloadSessionManager alloc] initWithDelegate:self];
    AppDelegate * myapp = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //NSLog(@"delegate dmanager = %@", myapp.dManager);
    dManager = myapp.dManager;
    [dManager setDelegate: self];
    
    //NSLog(@"media dmanager = %@", dManager);
    dManager.maxConcurrentDownloads = 1;
    [self setTitle:@"Download Progress"];
    //self.tabBarItem = [[[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemDownloads tag:0] autorelease];
    self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Downloads" image:[UIImage imageNamed:@"icondownload.png"] tag:333];
    
    
    [self refreshData];
    //NSLog(@"awwake from nib ");
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
        //dManager.maxConcurrentDownloads = 3;
        [self setTitle:@"Download Progress"];
        self.tabBarItem = [[[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemDownloads tag:0] autorelease];
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Downloads" image:[UIImage imageNamed:@"icondownload.png"] tag:1];
        [self refreshData];
       // NSLog(@"initWithStyle");
    }
    return self;
}
-(void) refreshFromUI
{
    [self refreshData];
    
    if ([self.refreshControl isRefreshing]) [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}
-(void) refreshData
{
    //NSLog(@"refreshData");
    [mediaMutableArray release];
    mediaMutableArray = nil;
    mediaMutableArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"arrayDownload"]];
    //NSLog(@"refresh data %@" , mediaMutableArray);
    

}
-(void) viewWillAppear:(BOOL)animated
{
    //NSLog(@"willAppear");
    
    [self refreshData];
    [self.tableView reloadData];
    
    [super viewWillAppear:animated];
}
- (void)viewDidLoad
{
    //NSLog(@"didload");
    [super viewDidLoad];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl setHidden:NO];
    [self.refreshControl addTarget:self action:@selector(refreshFromUI) forControlEvents:UIControlEventValueChanged];
    [self.refreshControl setAttributedTitle:[[NSAttributedString alloc] initWithString:@"pull down to refresh"]];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    //self.navigationItem.rightBarButtonItem = self.editButtonItem;
    //[self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(openHistory)]];
    NSData * greendata = [[NSUserDefaults standardUserDefaults] objectForKey:@"TintColor"];
    UIColor *greenish = [NSKeyedUnarchiver unarchiveObjectWithData:greendata];
    
   // [self.navigationItem setRightBarButtonItem: [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconhistory.png"] style:UIBarButtonItemStylePlain target:self action:@selector(openHistory)]];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"stop all" style:UIBarButtonItemStylePlain target:self action:@selector(tappedCancelButton:)]];
    [self.navigationItem.rightBarButtonItem setTintColor:greenish];
     
    [self.refreshControl setTintColor:greenish];
    
    
}
-(void) openHistory
{
    HistoryTVC * hist = [[HistoryTVC alloc] initWithStyle:UITableViewStylePlain];
    [self.navigationController pushViewController:hist animated:YES];
    [hist autorelease];
}
                                               
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //NSLog(@"rowcount");
    //return mediaMutableArray.count;
    return [dManager.downloads count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.currentIndex = indexPath.row;
    UIActionSheet * newAction = [[UIActionSheet alloc] initWithTitle:@"Press STOP to cancel Download" delegate:self cancelButtonTitle:@"close" destructiveButtonTitle:@"STOP" otherButtonTitles: nil];
    [newAction showFromTabBar: self.tabBarController.tabBar];
    [tableView reloadData];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DownloadCell";
    DownloadCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    DownloadSession *download = dManager.downloads[indexPath.row];
   // if( !download.isDownloading) [download start];
    cell.filenameLabel.text = [download.filename lastPathComponent];
    cell.urlLabel.text = [download.url absoluteString];
   // cell.totalLabel.text = [NSString stringWithFormat:@"%.2f %%", ([download progressContentLength]* 100.00) /([download expectedContentLength]*1.00) ];
   // cell.progressLabel.text = [NSString stringWithFormat:@"%lld/%lld",download.progressContentLength ,  download.expectedContentLength];
    
    [cell.totalLabel setText:download.progress.localizedAdditionalDescription];
    [cell.progressLabel setText:download.progress.localizedDescription];
    [cell.progressView setProgress:download.progress.fractionCompleted];
    
    
    if (download.isDownloading)
    {
        // if we're downloading a file turn on the activity indicator
        
        if (!cell.activityIndicator.isAnimating)
            [cell.activityIndicator startAnimating];
        
        cell.activityIndicator.hidden = NO;
        cell.progressView.hidden = NO;
        
        [self updateProgressViewForIndexPath:indexPath download:download];
    }
    else
    {
        // if not actively downloading, no spinning activity indicator view nor file download progress view is needed
        
        [cell.activityIndicator stopAnimating];
        cell.activityIndicator.hidden = YES;
        cell.progressView.hidden = YES;
    }
    
    return cell;
    /*
    static NSString *CellIdentifier = @"MediaCell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
    // Configure the cell...
    [cell.textLabel setText:[[mediaMutableArray objectAtIndex:indexPath.row] valueForKey:@"filename"]];
    [cell.detailTextLabel setText:[[mediaMutableArray objectAtIndex:indexPath.row] valueForKey:@"status"]];
    return cell;
     */
}
- (void)updateProgressViewForIndexPath:(NSIndexPath *)indexPath download:(DownloadSession *)download
{
    DownloadCell *cell = (DownloadCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    // if the cell is not visible, we can return
    
    if (!cell)
    {
        //NSLog(@"CELL IS NULLL ");
        
        return;
    }
    
    if (download.expectedContentLength >= 0)
    {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell.totalLabel setText:download.progress.localizedAdditionalDescription];
            [cell.progressLabel setText:download.progress.localizedDescription];
            [cell.progressView setProgress:download.progress.fractionCompleted];
        });
        
        
    }
    else
    {
        
        cell.progressView.progress = (double) (download.progressContentLength % 1000000L) / 1000000.0;
    }
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/
#pragma mark - Action Sheet
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
   // NSLog(@"Index Button %d, current index = %d", buttonIndex, currentIndex);
    if ( buttonIndex == 0) //button stop
    {
        DownloadSession * download = self.dManager.downloads[self.currentIndex];
        [download cancel];
    }
}
/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */
- (IBAction)tappedCancelButton:(id)sender
{
    [dManager cancelAll];
}

#pragma mark - Download Manager
/** Informs the delegate that a particular download has finished successfully.
 *
 * @param downloadManager
 *
 * The `DownloadManager` that is performing the downloads.
 *
 * @param download
 *
 * The individual `Download`.
 *
 * @see DownloadManager
 * @see Download
 */

- (void)DownloadSessionManager:(DownloadSessionManager *)downloadManager downloadDidFinishLoading:(DownloadSession *)download
{
    //NSLog(@"download did finish loading, %@", download.filename);
    [self.tableView reloadData];
    
}

/** Informs the delegate that a particular download has failed.
 *
 * @param downloadManager
 *
 * The `DownloadManager` that is performing the downloads.
 *
 * @param download
 *
 * The individual `Download`.
 *
 * @see DownloadManager
 * @see Download
 */

- (void)DownloadSessionManager:(DownloadSessionManager *)downloadManager downloadDidFail:(DownloadSession *)download
{
    ///NSLog(@"download fail , %@,error %@ ", download.filename, download.errorDownload.description);
    [self.tableView reloadData];
}

/** Informs the delegate of the status of a particular download that is in progress.
 *
 * @param downloadManager
 *
 * The `DownloadManager` that is performing the downloads.
 *
 * @param download
 *
 * The individual `Download`.
 *
 * @see DownloadManager
 * @see Download
 */


- (void)DownloadSessionManager:(DownloadSessionManager *)DownloadSessionManager2 downloadDidReceiveData:(DownloadSession *)download;
{
    NSLog(@"download progress %@, %lld", download, download.progressContentLength);
    for (NSInteger row = 0; row < [DownloadSessionManager2.downloads count]; row++)
    {
        if (download == DownloadSessionManager2.downloads[row])
        {
            
            [self updateProgressViewForIndexPath:[NSIndexPath indexPathForRow:row inSection:0] download:download];
            break;
        }
    }
}




@end
