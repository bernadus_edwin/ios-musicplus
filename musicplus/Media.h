//
//  Media.h
//  musicplus
//
//  Created by Erlangga on 10/9/13.
//  Copyright (c) 2013 Edward Julianto. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "DownloadManager.h"
#import "DownloadSessionManager.h"
#import "AFURLSessionManager.h"

@interface Media : UITableViewController<DownloadSessionManagerDelegate, UIActionSheetDelegate>
{
    NSMutableArray * mediaMutableArray;
    //DownloadManager * dManager;
    DownloadSessionManager * dManager;
    NSInteger  currentIndex;
    AFURLSessionManager * aManager;
}
@property (retain, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (nonatomic)     NSInteger  currentIndex;
@property (nonatomic, strong) NSMutableArray * mediaMutableArray;
//@property (nonatomic, strong) DownloadManager * dManager;
@property (nonatomic, strong) DownloadSessionManager * dManager;
@property (nonatomic, strong) AFURLSessionManager * aManager;
@end
