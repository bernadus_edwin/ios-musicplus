//
//  DownloadTVC.h
//  Music+
//
//  Created by EIT-MACMINI02 on 9/20/13.
//  Copyright (c) 2013 Trinity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface DownloadTVC : UITableViewController<UIDocumentInteractionControllerDelegate>
{
    AVAudioPlayer *audioPlayer;
    AVQueuePlayer * queuePlayer;
    //NSArray * paths;
    NSString * documentsDirectory;
    NSMutableArray * directoryContent;

}
//@property (nonatomic, retain) NSArray * paths;
@property (nonatomic, retain) NSString * documentsDirectory;
@property (nonatomic, retain) NSMutableArray * directoryContent;


 
@end
