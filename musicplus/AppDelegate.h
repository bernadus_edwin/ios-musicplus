//
//  AppDelegate.h
//  musicplus
//
//  Created by Erlangga on 9/29/13.
//  Copyright (c) 2013 Edward Julianto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DownloadManager.h"
#import "DownloadSessionManager.h"
#import <AVFoundation/AVFoundation.h>
#import "AFURLSessionManager.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UITabBarController *tabBarController;
@property (strong, nonatomic) DownloadSessionManager * dManager;

@property (strong, nonatomic) AVAudioPlayer * appAudioPlayer;

@property (copy) void (^backgroundSessionCompletionHandler)();
@end
