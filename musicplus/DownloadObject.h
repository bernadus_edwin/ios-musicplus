//
//  DownloadObject.h
//  musicplus
//
//  Created by Erlangga on 10/9/13.
//  Copyright (c) 2013 Edward Julianto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DownloadObject : NSObject
{
    NSString * __unsafe_unretained filename ;
    NSString * __unsafe_unretained status;
}
@property (nonatomic, assign) NSString * filename;
@property (nonatomic, assign) NSString * status;
@property (nonatomic, assign) NSString * stringURL;
@property (nonatomic, assign) NSString * stringDate;
-(id) initWith:(NSString *) filename Status:(NSString *) status;
-(id) initWith:(NSString *)strFilename Status:(NSString *)strStatus stringURL:(NSString *)strURL stringDate:(NSString *) strDate;
-(NSDictionary *) getDictionary;
@end
