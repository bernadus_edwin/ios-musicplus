//
//  DownloadTVC.m
//  Music+
//
//  Created by EIT-MACMINI02 on 9/20/13.
//  Copyright (c) 2013 Trinity. All rights reserved.
//

#import "DownloadTVC.h"
#import <MediaPlayer/MediaPlayer.h>
#import "avTouchViewController.h"
#import "AppDelegate.h"
@implementation DownloadTVC
@synthesize  documentsDirectory, directoryContent;
-(void)editMode:(id)sender
{
    if ([self.tableView isEditing])
    {
        
        [self.tableView setEditing:NO animated:YES];
        [self.navigationItem setRightBarButtonItem:[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editMode:)] autorelease]];
    }
    else
    {
        [self.tableView setEditing:YES animated:YES];
        [self.navigationItem setRightBarButtonItem:[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(editMode:)] autorelease]];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    //NSLog(@"out");
    if ([audioPlayer isPlaying])
    {
        [audioPlayer stop];
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    //[self refreshData];
    //[self.tableView reloadData];
    //[super viewDidAppear:animated];
    //[[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        [self setTitle:@"Saved Files"];
        [self setTabBarItem:[[UITabBarItem alloc] initWithTitle:@"Files" image:[UIImage imageNamed:@"iconfiles.png"] tag:3]];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nowplay.png" ] style:UIBarButtonItemStylePlain target:self action:@selector(showPlayer:)];
        
        NSArray * paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [[paths objectAtIndex:0] copy];
        
        //[directoryContent release];
        directoryContent = nil;
        directoryContent = [[NSMutableArray alloc ] initWithArray:[[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:nil]  copyItems:YES];
//        debug

        [self setRefreshControl:[[UIRefreshControl alloc] init]];

        //[self.refreshControl setAttributedTitle:[[NSAttributedString alloc] initWithString:@"loading"]];
        [self.refreshControl addTarget:self action:@selector(refreshsome) forControlEvents:UIControlEventValueChanged];
        NSData * greendata = [[NSUserDefaults standardUserDefaults] objectForKey:@"TintColor"];
        UIColor *greenish = [NSKeyedUnarchiver unarchiveObjectWithData:greendata];
        [self.refreshControl setTintColor:greenish];
        //[self.refreshControl setNeedsLayout];
  
    }
    return self;
}
-(void) refreshsome
{
    [self refreshData];
    [self.tableView reloadData];
    if ([self.refreshControl isRefreshing]) [self.refreshControl endRefreshing];

}
-(void) showPlayerWithIndexFile:(NSInteger) index
{
    avTouchViewController *player = [[avTouchViewController alloc] initWithNibName:@"avTouchViewController" bundle:[NSBundle mainBundle]];
    //[player setEmptyPlaylist:TRUE];
    [player setIndexFile:index];
    
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:player] animated:YES completion:^{ [player autoPlay];}];
    

}
-(IBAction)showPlayer:(id)sender
{
    avTouchViewController *player = [[avTouchViewController alloc] initWithNibName:@"avTouchViewController" bundle:[NSBundle mainBundle]];
    [player setEmptyPlaylist:TRUE];
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:player] animated:YES completion:^{}];
}

- (void)viewDidLoad
{
    //[self refreshData];
    
    NSError *categoryError;
    NSError *activationError;
    if (![[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&categoryError]) {
        // Handle the error here.
        NSLog(@"Audio Session error %@, %@", categoryError, [categoryError userInfo]);
    }
    else{
        if(![[AVAudioSession sharedInstance] setActive:YES error:&activationError])
        {
            NSLog(@"Audio Activation error %@, %@", activationError, [activationError userInfo]);
        } 
    }
    
    //NSLog(@"didload");
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    //self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
-(void)viewWillAppear:(BOOL)animated
{
    [ self refreshData];
    [self.tableView reloadData];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
    [super viewWillDisappear:animated];
}
-(BOOL)canBecomeFirstResponder
{
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSString * ) tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"Please use PC/MAC to copy saved files";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{     // Return the number of rows in the section.
    
     return directoryContent.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    [cell.textLabel setText:[[directoryContent objectAtIndex:indexPath.row] description]];
    return cell;
}
-(void) refreshData
{
    //NSLog(@"REFRESHING DATA %@", directoryContent);
    //paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    //documentsDirectory = [paths objectAtIndex:0];
   // [directoryContent release];
    //directoryContent = nil;
    
    directoryContent = [[NSMutableArray alloc ] initWithArray:[[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:nil]  copyItems:YES];
    
    
    
}
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source 
        NSURL * fileurl = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:[directoryContent objectAtIndex:indexPath.row]]];
        
        
         NSError * error ;
         if(![[NSFileManager defaultManager] removeItemAtURL:fileurl error:&error ])
         {
             //NSLog(@"failed removeItemAtPath : %@", error);
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete File" message:@"Delete file Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
             [alert show];
             [alert autorelease];
         }
        
        [directoryContent removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];


        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    
    //dah jalan
    AppDelegate * sharedApp = (AppDelegate * )[ [UIApplication sharedApplication] delegate];
    if(sharedApp.appAudioPlayer.isPlaying)
        [sharedApp.appAudioPlayer stop];
    [self showPlayerWithIndexFile:indexPath.row];
    
    
    //TESTING SHARE
    /*NSURL * fileurl = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:[directoryContent objectAtIndex:indexPath.row]]] ;
    UIDocumentInteractionController * con = [UIDocumentInteractionController interactionControllerWithURL:fileurl];
    [con setDelegate:self];
    [con presentPreviewAnimated:YES];
    */
    
    /*if (audioPlayer == nil)
    {
        audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileurl error:nil];
    }
    else
    {
        if ([audioPlayer isPlaying])
        {
            [audioPlayer stop];
            [audioPlayer release];
        }
        audioPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:fileurl error:nil];
    }
    [audioPlayer play];
    //TODO QUEUEING
     */
}
/*
- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    
}*/

@end
