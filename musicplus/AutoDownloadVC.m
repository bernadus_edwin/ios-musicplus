//
//  AutoDownloadVC.m
//  Music+
//
//  Created by EIT-MACMINI02 on 9/20/13.
//  Copyright (c) 2013 Trinity. All rights reserved.
//

#import "AutoDownloadVC.h"
#import "DownloadObject.h"
#import "AppDelegate.h"
 
@implementation AutoDownloadVC
@synthesize myPickerView,doneButton, cancelButton, urlString;
@synthesize count;
@synthesize renamedFile;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        count = 0;
        
    }
    return self;
}

- (void)viewDidLoad
{
    //NSLog(@"urlString did load ");
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//PICKERVIEW DataSource
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 3;
}

//PICKERVIEW DELEGATE

// returns width of column and height of row for each component.
//- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component;
//- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component;

// these methods return either a plain NSString, a NSAttributedString, or a view (e.g UILabel) to display the row for the component.
// for the view versions, we cache any hidden and thus unused views and pass them back for reuse.
// If you return back a different object, the old one will be released. the view will be centered in the row rect
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (row == 0)
    {
        return [NSString stringWithFormat: @"Download All (%d)", count];
    }
    else if (row == 1)
    {
        return @"Download And Rename";
    }
    else if (row == 2)
    {
        return @"Set As Home Page";
    }
    else
    {
        return nil;
    }

}
//- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component NS_AVAILABLE_IOS(6_0); // attributed title is favored if both methods are implemented
//- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view;

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}

-(void) sendDownloadToApp:(NSString *) filename
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setDoesRelativeDateFormatting:YES];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en/US"]];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date] ];
    
    DownloadObject * obj = [[DownloadObject alloc] initWith:filename Status:@"Downloading" stringURL:urlString stringDate:dateString];
    
    NSMutableArray * hist = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:@"histDownload"]];
    [hist addObject:[obj getDictionary]];
    [[NSUserDefaults standardUserDefaults] setObject:hist forKey:@"histDownload"];
    
    
    AppDelegate * myApp = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSURL * url = [NSURL URLWithString:urlString];
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSLog(@"saving file to %@", documentsDirectory);
    //make a file name to write the data to using the documents directory:
    NSString *destpath = [documentsDirectory stringByAppendingPathComponent:filename];
    if([[NSFileManager defaultManager] fileExistsAtPath:destpath])
    {//check file exist panggil auto increment
        destpath = [documentsDirectory stringByAppendingPathComponent:[self getNewFileIncremented:filename Dest:documentsDirectory]];
    }
    
    [myApp.dManager addDownloadWithFilename:destpath URL:url];
}
-(void) actionButton:(id)sender
{
    if ([sender isEqual:doneButton])
    {
        NSInteger currentIx = [myPickerView selectedRowInComponent:0];
        if (currentIx == 0) {
            //TODO disini mulai download
            [self sendDownloadToApp:[[urlString lastPathComponent] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//            [self performSelectorInBackground:@selector(bgprocess) withObject:nil];
            [self dismissViewControllerAnimated:YES completion:^{}];
        }
        else if( currentIx == 1 ) { // download and rename
            UIAlertView * renameAlert =[ [UIAlertView alloc] initWithTitle:@"Rename" message:@"rename the file" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Rename", nil];
            [renameAlert setAlertViewStyle:UIAlertViewStylePlainTextInput];
            NSArray *pathComp = [urlString pathComponents];
            NSString *filename = [[pathComp objectAtIndex:pathComp.count -1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            

            [[renameAlert textFieldAtIndex:0] setText:filename];
            [renameAlert show];
            [[renameAlert textFieldAtIndex:0] becomeFirstResponder];
            
        }
        

    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:^{}];

    
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        NSLog(@"result rename : %@" , [[alertView textFieldAtIndex:0] text]);
        renamedFile =[[ alertView textFieldAtIndex:0]text];
        if (! [[renamedFile pathExtension] isEqualToString:@"mp3"])
        {
            renamedFile = [[[alertView textFieldAtIndex:0] text] stringByAppendingPathExtension:@"mp3"];
        }
        [self addDownloadData:renamedFile];
        //[self performSelectorInBackground:@selector(bgrenameprocess) withObject:nil];
        [self dismissViewControllerAnimated:YES completion:^{}];
    }
}

-(void) bgrenameprocess
{
    NSString * filename = renamedFile;
    [self addDownloadData:filename];
    
    //DOWNLOAD KE NSDATA
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
    
    //TODO nambah query ke server brp gede datanya ( workflow hrus di edit )
    
    
    [self writeFileWithData:data Named:filename];
    UIAlertView *view = [[UIAlertView alloc] initWithTitle:@"Download" message:@"download complete" delegate:nil cancelButtonTitle:@"close" otherButtonTitles:nil];
    
    [view show];
}
-(void) bgprocess//download 1 file
{
    NSArray *pathComp = [urlString pathComponents];
    NSString *filename = [[pathComp objectAtIndex:pathComp.count -1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self addDownloadData:filename];
    
    //DOWNLOAD KE NSDATA
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
    
    //TODO nambah query ke server brp gede datanya ( workflow hrus di edit )
    
    
    [self writeFileWithData:data Named:filename];
    UIAlertView *view = [[UIAlertView alloc] initWithTitle:@"Download" message:@"download complete" delegate:nil cancelButtonTitle:@"close" otherButtonTitles:nil];
    
    [view show]; 
}
-(void) addDownloadData :(NSString * )filename
{
    
    
    NSMutableArray * arrayDownload = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:@"arrayDownload"]];
    //DownloadObject * obj = [[DownloadObject alloc] initWith:filename Status:@"Downloading"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setDoesRelativeDateFormatting:YES];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en/US"]];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date] ];
    DownloadObject * obj = [[DownloadObject alloc] initWith:filename Status:@"Downloading" stringURL:urlString stringDate:dateString];
    

    [arrayDownload addObject:[obj getDictionary]];
    [[NSUserDefaults standardUserDefaults] setObject:arrayDownload  forKey:@"arrayDownload"];
    NSLog(@"add download data %@", arrayDownload);

}

-(NSString *) getNewFileIncremented:(NSString* ) filename Dest:(NSString *) documentDirectory
{
    //add "_#" to file name
    //for # = 1 sd unlimited, rewrite #
    //try save _#
    NSInteger index = 1;
    NSString * newfilename =  [NSString stringWithFormat:@"%@-%d.%@", [filename stringByDeletingPathExtension], index, [filename pathExtension]];
    NSString * destpath = [documentDirectory stringByAppendingPathComponent:newfilename];
    while ([[NSFileManager defaultManager] fileExistsAtPath:destpath]) {
        newfilename = [NSString stringWithFormat:@"%@-%d.%@", [filename stringByDeletingPathExtension], index++, [filename pathExtension]];
        destpath = [documentDirectory stringByAppendingPathComponent:newfilename];
    }
    return newfilename;
}
-(void) writeFileWithData:(NSData *) data Named:(NSString *)filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSLog(@"saving file to %@", documentsDirectory);
    //make a file name to write the data to using the documents directory:
    NSString *destpath = [documentsDirectory stringByAppendingPathComponent:filename];
    if([[NSFileManager defaultManager] fileExistsAtPath:destpath])
    {//check file exist panggil auto increment
        destpath = [documentsDirectory stringByAppendingPathComponent:[self getNewFileIncremented:filename Dest:documentsDirectory]];
    }
    [data writeToFile:destpath atomically:YES];
    
    NSMutableArray * arrayDownload = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:@"arrayDownload"]];
    for (int i= 0; i< arrayDownload.count; i++) {
        if ([[((NSDictionary *) [arrayDownload objectAtIndex:i] )objectForKey:@"filename"  ]isEqualToString:filename]) {
            //remove array
            NSMutableArray * hist = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:@"histDownload"]];
            [hist addObject:[arrayDownload objectAtIndex:i]];
            [[NSUserDefaults standardUserDefaults] setObject:hist forKey:@"histDownload"];
            [arrayDownload removeObjectAtIndex:i];
            [[NSUserDefaults standardUserDefaults] setObject:arrayDownload  forKey:@"arrayDownload"];
            break;
        }
    }
    
    
    //[self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:destpath]];
    
}
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}
@end
