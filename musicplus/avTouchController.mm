	/*
 
 File: avTouchController.mm
 Abstract: n/a
 Version: 1.4.1
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2010 Apple Inc. All Rights Reserved.
 
 
 */


#import "avTouchController.h"
//#include "CALevelMeter.h"
#include "AppDelegate.h"
// amount to skip on rewind or fast forward
#define SKIP_TIME 1.0			
// amount to play between skips
#define SKIP_INTERVAL .2

@implementation avTouchController
@synthesize playlist;
@synthesize isPlaylistEmpty;
@synthesize indexPlay;
@synthesize fileName;
@synthesize playButton;
@synthesize ffwButton;
@synthesize rewButton;
@synthesize volumeSlider;
@synthesize progressBar;
@synthesize currentTime;
@synthesize duration;
@synthesize lvlMeter_in;
@synthesize artwork;
@synthesize updateTimer;
@synthesize player;
@synthesize nextButton;
@synthesize prevButton;

@synthesize inBackground;
@synthesize parentVC;
@synthesize repeatButton, shuffleButton;
@synthesize isNewPlay;

void RouteChangeListener(	void *                  inClientData,
							AudioSessionPropertyID	inID,
							UInt32                  inDataSize,
							const void *            inData);


/*
- (void) setIndexPlay:(NSInteger) indexPlayed
{
    [self setIndexPlay:indexPlayed];
}*/
- (void)metadata:(NSURL*)fileURL {

    AVURLAsset *asset = [AVURLAsset URLAssetWithURL: fileURL options:nil];
    
    NSArray *titles = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata withKey:AVMetadataCommonKeyTitle keySpace:AVMetadataKeySpaceCommon];
    NSArray *artists = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata withKey:AVMetadataCommonKeyArtist keySpace:AVMetadataKeySpaceCommon];
    NSArray *albumNames = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata withKey:AVMetadataCommonKeyAlbumName keySpace:AVMetadataKeySpaceCommon];
    
    
    AVMetadataItem *title;
    if (titles.count > 0 ) title = [titles objectAtIndex:0];
    else title = [[AVMetadataItem alloc] init];
    
    AVMetadataItem *artist ;
    if (artists.count > 0 ) artist = [artists objectAtIndex:0];
    else artist = [[AVMetadataItem alloc] init];
    
    AVMetadataItem *albumName;
    if (albumNames.count > 0 ) albumName = [albumNames objectAtIndex:0];
    else albumName = [[AVMetadataItem alloc] init];
    
//    NSLog(@"title %@, artist %@, album %@", title, artist, albumName);
    /*
    NSArray *keys = [NSArray arrayWithObjects:@"commonMetadata", nil];
    [asset loadValuesAsynchronouslyForKeys:keys completionHandler:^{
        NSArray *artworks = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata
                                                           withKey:AVMetadataCommonKeyArtwork
                                                          keySpace:AVMetadataKeySpaceCommon];
       // NSLog(@"%@", artworks);
        for (AVMetadataItem *item in artworks) {
            if ([item.keySpace isEqualToString:AVMetadataKeySpaceID3]) {
                NSDictionary *d = [item.value copyWithZone:nil];
                [artwork setImage:[UIImage imageWithData:[d objectForKey:@"data"]]];
            } else if ([item.keySpace isEqualToString:AVMetadataKeySpaceiTunes]) {
                 [artwork setImage: [UIImage imageWithData:[item.value copyWithZone:nil]]];
            }
        }
    }];
    */
    
    /*
    self.currentSongTitle = [title.value copyWithZone:nil];
    self.currentSongArtist = [artist.value copyWithZone:nil];
    self.currentSongAlbumName = [albumName.value copyWithZone:nil];
    self.currentSongDuration = self.audioPlayer.duration;
     */
}

-(void)updateCurrentTimeForPlayer:(AVAudioPlayer *)p
{
	currentTime.text = [NSString stringWithFormat:@"%d:%02d", (int)p.currentTime / 60, (int)p.currentTime % 60, nil];
	progressBar.value = p.currentTime;
}

- (void)updateCurrentTime
{
	[self updateCurrentTimeForPlayer:self.player];
}

- (void)updateViewForPlayerState:(AVAudioPlayer *)p
{
	[self updateCurrentTimeForPlayer:p];

	if (updateTimer) 
		[updateTimer invalidate];
		
	if (p.playing)
	{
		[playButton setImage:((p.playing == YES) ? pauseBtnBG : playBtnBG) forState:UIControlStateNormal];
//		[lvlMeter_in setPlayer:p];
		updateTimer = [NSTimer scheduledTimerWithTimeInterval:.01 target:self selector:@selector(updateCurrentTime) userInfo:p repeats:YES];
	}
	else
	{
		[playButton setImage:((p.playing == YES) ? pauseBtnBG : playBtnBG) forState:UIControlStateNormal];
//		[lvlMeter_in setPlayer:nil];
		updateTimer = nil;
	}
	
}

- (void)updateViewForPlayerStateInBackground:(AVAudioPlayer *)p
{
	[self updateCurrentTimeForPlayer:p];
	
	if (p.playing)
	{
		[playButton setImage:((p.playing == YES) ? pauseBtnBG : playBtnBG) forState:UIControlStateNormal];
	}
	else
	{
		[playButton setImage:((p.playing == YES) ? pauseBtnBG : playBtnBG) forState:UIControlStateNormal];
	}
	if(p.numberOfLoops == 0)
       [repeatButton setSelected:NO];
    else
        [repeatButton setSelected:YES];
    
    
}

-(void)updateViewForPlayerInfo:(AVAudioPlayer*)p
{
	duration.text = [NSString stringWithFormat:@"%d:%02d", (int)p.duration / 60, (int)p.duration % 60, nil];
	progressBar.maximumValue = p.duration;
//	volumeSlider.value = p.volume;
}

- (void)rewind
{
	AVAudioPlayer *p = rewTimer.userInfo;
	p.currentTime-= SKIP_TIME;
	[self updateCurrentTimeForPlayer:p];
}

- (void)ffwd
{
	AVAudioPlayer *p = ffwTimer.userInfo;
	p.currentTime+= SKIP_TIME;	
	[self updateCurrentTimeForPlayer:p];
}


- (void)awakeFromNib
{
    [volumeSlider setMinimumVolumeSliderImage:[UIImage imageNamed:@"base2"] forState:UIControlStateNormal];
    [volumeSlider setMaximumVolumeSliderImage:[UIImage imageNamed:@"base1"] forState:UIControlStateNormal];
    [volumeSlider setRouteButtonImage:[UIImage imageNamed:@"iconmore"] forState:UIControlStateNormal];
    [volumeSlider sizeToFit];
    [progressBar setMinimumTrackImage:[UIImage imageNamed:@"base2"] forState:UIControlStateNormal];
    [progressBar setMaximumTrackImage:[UIImage imageNamed:@"base1"] forState:UIControlStateNormal];
    
    

    self.indexPlay = [parentVC getTableIndexPlay];
	playBtnBG = [[UIImage imageNamed:@"iconplay1.png"] retain];
	pauseBtnBG = [[UIImage imageNamed:@"iconstop.png"] retain];

	[playButton setImage:playBtnBG forState:UIControlStateNormal];
	
	//[self registerForBackgroundNotifications];
			
	updateTimer = nil;
	rewTimer = nil;
	ffwTimer = nil;
	
	duration.adjustsFontSizeToFitWidth = YES;
	currentTime.adjustsFontSizeToFitWidth = YES;
	progressBar.minimumValue = 0.0;	
	
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentDirectory =[[paths objectAtIndex:0] copy];

    playlist = [[[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentDirectory error:nil] copy]; 
    
    if( playlist.count != 0 && playlist.count >= indexPlay)
    {
        [self playIndex:indexPlay];
        
    }
    
	// Load the the sample file, use mono or stero sample
	//NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: [[NSBundle mainBundle] pathForResource:@"sample" ofType:@"m4a"]];
    /*if (!isPlaylistEmpty)
    {
    NSArray * paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *  documentDirectory  = [[paths objectAtIndex:0] copy];
    NSArray * files =[[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentDirectory error:nil] ;
    if( files.count > 0)
    {
    NSString * filename  =[files objectAtIndex:1];
    NSURL * fileURL = [[NSURL alloc] initFileURLWithPath:[documentDirectory stringByAppendingPathComponent:filename]];
   
    //NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: [[NSBundle mainBundle] pathForResource:@"sample2ch" ofType:@"m4a"]];

	self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];	
	if (self.player)
	{
		fileName.text = [NSString stringWithFormat: @"%@ (%d ch.)", [[player.url relativePath] lastPathComponent], player.numberOfChannels, nil];
		[self updateViewForPlayerInfo:player];
		[self updateViewForPlayerState:player];
        [self metadata:fileURL];
		player.numberOfLoops = 1;
		player.delegate = self;
	}
	 */
    
    
	
    
	
//    }

//    }

}
-(void) playIndex:(NSInteger) inPlayIndex
{
    [parentVC.gallery scrollToItemAtIndex:inPlayIndex animated:YES];
    
    if ([self.player isPlaying]) [self.player stop];
    NSString * filename  =[playlist objectAtIndex:inPlayIndex];
    //NSLog(@"playlist = %@", playlist);
    //NSLog(@"filename = %@", filename);
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentDirectory =[[paths objectAtIndex:0] copy];
    NSURL * fileURL = [[NSURL alloc] initFileURLWithPath:[documentDirectory stringByAppendingPathComponent:[filename stringByRemovingPercentEncoding]]];
    
    //NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: [[NSBundle mainBundle] pathForResource:@"sample2ch" ofType:@"m4a"]];
    NSError *error;
//
    /*
     open sesuatu 
     if play next then re-init
     
     
     
     
     */
    
    AppDelegate * sharedApp = (AppDelegate *)[ [UIApplication sharedApplication] delegate];
    if (sharedApp.appAudioPlayer == nil )
        sharedApp.appAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
    
    
    
    //kalo file yang diplay beda sama yang mau di play maka stop
   // if(![sharedApp.appAudioPlayer.url isEqual:fileURL])
    if(![sharedApp.appAudioPlayer isPlaying])
    {
        
        sharedApp.appAudioPlayer=[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
    
    }
    else
    {
        [self updateViewForPlayerState:sharedApp.appAudioPlayer];
        [self updateViewForPlayerInfo:sharedApp.appAudioPlayer];
    }
    //kalo background mau buka ini lg maka goto current player
    
    
    player = sharedApp.appAudioPlayer;
    //self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
    
	if (self.player)
	{
		fileName.text = [NSString stringWithFormat: @"%@ (%d ch.)", [[player.url relativePath] lastPathComponent], player.numberOfChannels, nil];
		[self updateViewForPlayerInfo:player];
		[self updateViewForPlayerState:player];
        [self metadata:fileURL];
        [player setDelegate:self];
        if (repeatButton.selected)
        {
            [player setNumberOfLoops:-1];
        }
        else
        {
            [player setNumberOfLoops:0];
        }
	}
    [self registerForBackgroundNotifications];

}
-(void)pausePlaybackForPlayer:(AVAudioPlayer*)p
{
	[p pause];
	[self updateViewForPlayerState:p];
}

-(void)startPlaybackForPlayer:(AVAudioPlayer*)p
{
	if ([p play])
	{
		[self updateViewForPlayerState:p];
        
		[self updateViewForPlayerInfo:player];
	}
	else
		NSLog(@"Could not play %@\n", p.url);
}

- (IBAction)playButtonPressed:(UIButton *)sender
{
	if (player.playing == YES)
		[self pausePlaybackForPlayer: player];
	else
		[self startPlaybackForPlayer: player];
}

- (IBAction)nextButtonPressed:(UIButton *) sender
{
    if ( shuffleButton.selected )
    {
        indexPlay = [self nextIndex];
        [self playIndex:indexPlay];
        
    }
    else
    {
        if (indexPlay < playlist.count-1)
        {
            indexPlay +=1;
            [self playIndex:indexPlay];
        }
    }
}
- (IBAction)prevButtonPressed:(UIButton *) sender
{
    if (shuffleButton.selected)
    {
        indexPlay = [self nextIndex];
        [self playIndex:indexPlay];
    }
    else
    {
        if (indexPlay > 0)
        {
            indexPlay -= 1;
            [self playIndex:indexPlay];
        }
    }
}
- (IBAction)rewButtonPressed:(UIButton *)sender
{
	if (rewTimer) [rewTimer invalidate];
	rewTimer = [NSTimer scheduledTimerWithTimeInterval:SKIP_INTERVAL target:self selector:@selector(rewind) userInfo:player repeats:YES];
}

- (IBAction)rewButtonReleased:(UIButton *)sender
{
	if (rewTimer) [rewTimer invalidate];
	rewTimer = nil;
}

- (IBAction)ffwButtonPressed:(UIButton *)sender
{
	if (ffwTimer) [ffwTimer invalidate];
	ffwTimer = [NSTimer scheduledTimerWithTimeInterval:SKIP_INTERVAL target:self selector:@selector(ffwd) userInfo:player repeats:YES];
}

- (IBAction)ffwButtonReleased:(UIButton *)sender
{
	if (ffwTimer) [ffwTimer invalidate];
	ffwTimer = nil;
}

- (IBAction)volumeSliderMoved:(UISlider *)sender
{
	player.volume = [sender value];
}

- (IBAction)progressSliderMoved:(UISlider *)sender
{
	player.currentTime = sender.value;
	[self updateCurrentTimeForPlayer:player];
}
- (IBAction) setRepeat:(id)sender
{
    [repeatButton setSelected:!repeatButton.selected];
    if (repeatButton.selected)
    {
        [player setNumberOfLoops:-1];
    }
    else
    {
        [player setNumberOfLoops:0];
    }
}
- (IBAction) setShuffle:(id)sender
{
    [shuffleButton setSelected:!shuffleButton.selected];
}
- (void)dealloc
{
	[super dealloc];
	
	[fileName release];
	[playButton release];
	[ffwButton release];
	[rewButton release];
	[volumeSlider release];
	[progressBar release];
	[currentTime release];
	[duration release];
	[lvlMeter_in release];
	
	[updateTimer release];
	[player release];
	
	[playBtnBG release];
	[pauseBtnBG release];	
}

#pragma mark AudioSession handlers

void RouteChangeListener(	void *                  inClientData,
							AudioSessionPropertyID	inID,
							UInt32                  inDataSize,
							const void *            inData)
{
	avTouchController* This = (avTouchController*)inClientData;
	
	if (inID == kAudioSessionProperty_AudioRouteChange) {
		
		CFDictionaryRef routeDict = (CFDictionaryRef)inData;
		NSNumber* reasonValue = (NSNumber*)CFDictionaryGetValue(routeDict, CFSTR(kAudioSession_AudioRouteChangeKey_Reason));
		
		int reason = [reasonValue intValue];

		if (reason == kAudioSessionRouteChangeReason_OldDeviceUnavailable) {

			[This pausePlaybackForPlayer:This.player];
		}
	}
}

-(NSInteger) nextIndex
{
    if( shuffleButton.selected )
        return arc4random()% playlist.count;
    else
        return indexPlay+=1;
}
#pragma mark AVAudioPlayer delegate methods

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)p successfully:(BOOL)flag
{
	if (flag == NO)
		NSLog(@"Playback finished unsuccessfully");
    
    
    
    
	[p setCurrentTime:0.];
    if (shuffleButton.selected)
    {
        indexPlay = [self nextIndex];
        [self playIndex:indexPlay];
        [self startPlaybackForPlayer:player];
    }
    else if (indexPlay < playlist.count -1)
    {
        indexPlay = [self nextIndex];
        [self playIndex:indexPlay];
        [self startPlaybackForPlayer:player];
    }
    
    else
    {
        if (inBackground)
        {
            [self updateViewForPlayerStateInBackground:p];
        }
        else
        {
            [self updateViewForPlayerState:p];
        }
    }
}

- (void)playerDecodeErrorDidOccur:(AVAudioPlayer *)p error:(NSError *)error
{
	NSLog(@"ERROR IN DECODE: %@\n", error); 
}

// we will only get these notifications if playback was interrupted
- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)p
{
	NSLog(@"Interruption begin. Updating UI for new state");
	// the object has already been paused,	we just need to update UI
	if (inBackground)
	{
		[self updateViewForPlayerStateInBackground:p];
	}
	else
	{
		[self updateViewForPlayerState:p];
	}
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)p
{
	NSLog(@"Interruption ended. Resuming playback");
	[self startPlaybackForPlayer:p];
}

#pragma mark background notifications
- (void)registerForBackgroundNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];

	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(setInBackgroundFlag)
												 name:UIApplicationWillResignActiveNotification
											   object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(clearInBackgroundFlag)
												 name:UIApplicationWillEnterForegroundNotification
											   object:nil];
    
    NSError * categoryError;
        NSError * activationError;
    if (![[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&categoryError]) {
        // Handle the error here.
        NSLog(@"Audio Session error %@, %@", categoryError, [categoryError userInfo]);
        
    }
    else{
        if(![[AVAudioSession sharedInstance] setActive:YES error:&activationError])
        {
            NSLog(@"Audio Activation error %@, %@", activationError, [activationError userInfo]);
            
        } 
    }

}

- (void)setInBackgroundFlag
{
	inBackground = true;
}

- (void)clearInBackgroundFlag
{
	inBackground = false;
}

@end