//
//  BrowserVC.m
//  Music+
//
//  Created by EIT-MACMINI02 on 9/20/13.
//  Copyright (c) 2013 Trinity. All rights reserved.
//

#import "BrowserVC.h"
 
#import "BookmarkTVC.h"
#import "avTouchViewController.h"
#import "DownloadObject.h"
#import "AppDelegate.h"
@implementation BrowserVC
@synthesize goButton,homeButton,myWebView, urlField, backButton, forwardButton, bookmarkButton, reloadButton, sheetButton,actionSheet, stopButton, loadingView;
@synthesize toolbarArray,toolbar, showButton, hideButton;
@synthesize urlString;
@synthesize progressBar, expectedProgress;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setTitle:@"Browse"]; 
        
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
  
    progressBar =[[UIProgressView alloc] initWithFrame:CGRectMake(0,20+44 , self.navigationController.view.bounds.size.width, 5)];
    
    [self.navigationController.view addSubview:progressBar];
//    self.tabBarItem = [[[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemSearch tag:0] autorelease];
    self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Browse" image:[UIImage imageNamed:@"iconbrowse.png"] tag:0];
    //[progressBar setProgress:0.0];
    //[progressBar setHidden:YES];

//NSAssert(self.backButton, @"Unconnected IBOutlet 'backButton'");
    //[self.navigationItem setRightBarButtonItem:goButton];
    [self.navigationItem setTitleView:urlField];
    //[self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:loadingView]];
    // Do any additional setup after loading the view from its nib.
    //[self setNeedsStatusBarAppearanceUpdate];
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemPlay target:self action:@selector(showPlayer:)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nowplay" ] style:UIBarButtonItemStylePlain target:self action:@selector(showPlayer:)];
    
    toolbarArray = [[NSMutableArray alloc ] initWithArray:[toolbar items] copyItems:NO];
    [toolbarArray removeObject:stopButton];
    [toolbarArray removeObject:showButton];
    UIColor * silverColor = [UIColor colorWithRed:0.80000001192092896 green:0.80000001192092896 blue:0.80000001192092896 alpha:1];
    // <color key="backgroundColor" red="0.80000001192092896" green="0.80000001192092896" blue="0.80000001192092896" alpha="1" colorSpace="calibratedRGB"/>
    [toolbar setBarTintColor:silverColor];
    UIColor * tungsten = [UIColor colorWithRed:0.20000000298023224 green:0.20000000298023224 blue:0.20000000298023224 alpha:1];
    [toolbar setTintColor:tungsten];
    [toolbar setItems:toolbarArray];
    [progressBar setTintColor:tungsten];
    [self updateButtons];
    //AUTO GO TO HOME[self myHome:nil];
}
-(IBAction) showPlayer:(id) sender
{
    avTouchViewController *player = [[avTouchViewController alloc] initWithNibName:@"avTouchViewController" bundle:[NSBundle mainBundle]];
    [player setEmptyPlaylist:TRUE];
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:player] animated:YES completion:^{}];
}

-(IBAction)toogleTabbar:(id)sender
{
    /*for(UIView *view in self.tabBarController.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, 580, view.frame.size.width,
                                      view.frame.size.height)];
        }
        else
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y,
                                      view.frame.size.width, view.frame.size.height +40)];
        }
    }*/
    //NSLog(@" screeen %@, tabbar %@, screen %@", self.tabBarController.tabBar.frame , self.view.frame,          [UIScreen mainScreen].bounds);
    CGRect  tabarframe= self.tabBarController.tabBar.frame;
    CGRect  viewframe = self.view.frame;
    
    //NSLog(@"%@, %@, %@", tabarframe, viewframe, screenframe);
    [self.tabBarController.tabBar setHidden:!self.tabBarController.tabBar.hidden];
 
    if(self.tabBarController.tabBar.hidden)
    {
        [self.view setFrame:CGRectMake(viewframe.origin.x, viewframe.origin.y, viewframe.size.width, viewframe.size.height + tabarframe.size.height)];
    }
    else
    {
        
        [self.view setFrame:CGRectMake(viewframe.origin.x, viewframe.origin.y, viewframe.size.width, viewframe.size.height - tabarframe.size.height)];
    }
    [self setImageForToolbarToogle:self.tabBarController.tabBar.hidden];
    
}
-(void) setImageForToolbarToogle:(BOOL) hidden
{
    if (hidden)
    {
        NSUInteger index2 = [toolbarArray indexOfObject:self.hideButton];
        if (index2 < toolbarArray.count)
            [toolbarArray replaceObjectAtIndex:index2 withObject:showButton];
         
    }
    else
    {
        NSUInteger index = [toolbarArray indexOfObject:self.showButton];
        if (index < toolbarArray.count)
            [toolbarArray replaceObjectAtIndex:index withObject:hideButton];
        
    }
    [toolbar setItems:toolbarArray];
}
-(IBAction) myBack:(id)sender
{
    [myWebView goBack];
}
-(IBAction) myForward:(id)sender
{
    [myWebView goForward];
}
-(IBAction) myStop:(id)sender
{
    [myWebView stopLoading];
}
-(IBAction) myRefresh:(id)sender
{
    [myWebView reload];
}

-(IBAction) myHome:(id)sender{
    //TODO gohome
    [urlField setText:@"http://www.google.com"];

   [self textFieldShouldReturn:urlField];
    
    
    /*NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"index"];
    NSString * htmlPath = [documentsDirectory stringByAppendingPathComponent:@"index.html"];
    NSURL * htmlURL = [NSURL fileURLWithPath:htmlPath];
    NSURLRequest * req  = [[NSURLRequest alloc] initWithURL:htmlURL];
   
    [myWebView loadRequest:req];*/

    
    
}
-(IBAction) myBookmark:(id)sender
{
    //TODO open bookmark
}
-(IBAction) myAutoDownload:(id)sender
{
    //TODO generate list auto download
}
-(void)viewWillAppear:(BOOL)animated
{
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//CUSTOM
-(IBAction)showActionSheet:(id)sender
{
    
}
-(IBAction)showBookmarkVC:(id)sender
{

    BookmarkTVC * bv = [[BookmarkTVC alloc] initWithStyle:UITableViewStyleGrouped]  ;
    [bv setStringURL:myWebView.request.URL.absoluteString];
        UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:bv];
    [bv setParent:self];
    [self presentViewController:nav animated:YES completion:^{
        NSLog(@"presented");
    }];
}

-(IBAction)showAutoDownload:(id)sender
{
    self.urlString = [downloadString copy];
    
    /*advc = [[AutoDownloadVC alloc] initWithNibName:@"AutoDownloadVC" bundle:[NSBundle mainBundle]] ;
    //[advc setUrlString: downloadString ];
    [advc setCount:1];
    [advc setUrlString:[downloadString copy]];
    [advc setModalTransitionStyle:UIModalTransitionStylePartialCurl];
    [advc setModalPresentationStyle:UIModalPresentationFormSheet];
    
    [self presentViewController:advc animated:YES completion:^{
        
    }];*/
    UIActionSheet * dlAct = [[UIActionSheet alloc] initWithTitle:@"MP3 Media Found" delegate:self cancelButtonTitle:@"cancel" destructiveButtonTitle:nil otherButtonTitles:@"Download",@"Download and Rename", nil];
    [dlAct showFromTabBar:self.tabBarController.tabBar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        [self sendDownloadToApp:[[urlString lastPathComponent] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
    if (buttonIndex == 1)
    {
        UIAlertView * renameAlert =[ [UIAlertView alloc] initWithTitle:@"Rename" message:@"rename the file" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Rename", nil];
        [renameAlert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        NSArray *pathComp = [urlString pathComponents];
        NSString *filename = [[pathComp objectAtIndex:pathComp.count -1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        [[renameAlert textFieldAtIndex:0] setText:filename];
        [renameAlert show];
        [[renameAlert textFieldAtIndex:0] becomeFirstResponder];
        [[renameAlert textFieldAtIndex:0]selectAll:self];
        UITextPosition * begin = [[renameAlert textFieldAtIndex:0] positionFromPosition:[[renameAlert textFieldAtIndex:0]beginningOfDocument] offset:0];
        UITextPosition * finish = [[renameAlert textFieldAtIndex:0] positionFromPosition:[[renameAlert textFieldAtIndex:0] endOfDocument] offset:-4];
        UITextRange * selectedfilter = [[renameAlert textFieldAtIndex:0] textRangeFromPosition:begin toPosition:finish];
        [[renameAlert textFieldAtIndex:0] setSelectedTextRange:selectedfilter];
        
        [UIMenuController sharedMenuController].menuVisible = NO;
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        NSLog(@"result rename : %@" , [[alertView textFieldAtIndex:0] text]);
        NSString * renamedFile =[[ alertView textFieldAtIndex:0]text];
        if (! [[renamedFile pathExtension] isEqualToString:@"mp3"])
        {
            renamedFile = [[[alertView textFieldAtIndex:0] text] stringByAppendingPathExtension:@"mp3"];
        }
        [self sendDownloadToApp:renamedFile];
        
    }
}


-(void) sendDownloadToApp:(NSString *) filename
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setDoesRelativeDateFormatting:YES];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en/US"]];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date] ];
    
    DownloadObject * obj = [[DownloadObject alloc] initWith:filename Status:@"Downloading" stringURL:urlString stringDate:dateString];
    
    NSMutableArray * hist = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:@"histDownload"]];
    [hist addObject:[obj getDictionary]];
    [[NSUserDefaults standardUserDefaults] setObject:hist forKey:@"histDownload"];
    
    
    AppDelegate * myApp = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSURL * url = [NSURL URLWithString:urlString];
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSLog(@"saving file to %@", documentsDirectory);
    //make a file name to write the data to using the documents directory:
    NSString *destpath = [documentsDirectory stringByAppendingPathComponent:filename];
    
    [myApp.dManager addDownloadWithFilename:destpath URL:url];
    if ( [myApp.dManager countActiveDownloads ]  == 0 )
    {
        [myApp.dManager start];
    }
    
}
-(NSString *) getNewFileIncremented:(NSString* ) filename Dest:(NSString *) documentDirectory
{
    //add "_#" to file name
    //for # = 1 sd unlimited, rewrite #
    //try save _#
    NSInteger index = 1;
    NSString * newfilename =  [NSString stringWithFormat:@"%@-%d.%@", [filename stringByDeletingPathExtension], index, [filename pathExtension]];
    NSString * destpath = [documentDirectory stringByAppendingPathComponent:newfilename];
    while ([[NSFileManager defaultManager] fileExistsAtPath:destpath]) {
        newfilename = [NSString stringWithFormat:@"%@-%d.%@", [filename stringByDeletingPathExtension], index++, [filename pathExtension]];
        destpath = [documentDirectory stringByAppendingPathComponent:newfilename];
    }
    return newfilename;
}
-(void)updateButtons
{
    backButton.enabled = self.myWebView.canGoBack;
    [forwardButton setEnabled:self.myWebView.canGoForward];
    [stopButton setEnabled:self.myWebView.loading];
    
    if (self.myWebView.loading )
    {
        NSUInteger index2 = [toolbarArray indexOfObject:self.reloadButton];
        if (index2 < toolbarArray.count)
            [toolbarArray replaceObjectAtIndex:index2 withObject:stopButton];
    
        //[toolbarArray replaceObjectAtIndex:4 withObject:stopButton];
    }
    else
    {
        NSUInteger index = [toolbarArray indexOfObject:self.stopButton];
        if (index < toolbar.items.count)
            [toolbarArray replaceObjectAtIndex: index withObject:reloadButton];
        
    }
    [toolbar setItems:toolbarArray];

}
//UIWEBVIEWDELEGATE
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [loadingView startAnimating];
    [progressBar setHidden:NO];
    [self updateButtons];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
//    [urlField setText:[webView stringByEvaluatingJavaScriptFromString:@"document.title"]];
    if(![webView.request.URL.scheme isEqualToString:@"file"])
        [urlField setText:webView.request.URL.absoluteString];
    [loadingView stopAnimating];
    [progressBar setHidden:YES];
    [self updateButtons];
}
//UITEXTFIELD DELEGATE
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField.text isEqualToString:@""])
        [textField setText:@"http://"];
    if ([textField.text isEqualToString:@"about:blank"])
        [textField setText:@"http://"];
    [textField selectAll:self];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    prevString = textField.text.copy;
    [self loadwebrequest];
    return YES;
}
/*
-(void) webviewloadwithtrap
{
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlField.text]];
    
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [conn start];
    
    
}
 */
-(void) shouldLoadWebRequest:(NSString *) val
{
    [self.urlField setText:val];
    [self textFieldShouldReturn:urlField];
}
-(void) loadwebrequest
{
    
    
    [urlField resignFirstResponder];
    NSURL * url = [NSURL URLWithString:prevString];
    if(!url.scheme)
    {
        NSString* modifiedURLString = [NSString stringWithFormat:@"http://%@", prevString];
        url = [NSURL URLWithString:modifiedURLString];
    }
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
   // NSURLConnection *conn;
    //conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
    
    /*if (![NSURLConnection canHandleRequest:request])
    {
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",urlField.text]]];
        if( ![NSURLConnection canHandleRequest:request])
        {
            NSString* newText = (NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)urlField.text,NULL,(CFStringRef)@"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8);
        
            NSString *googlestring = [NSString stringWithFormat:@"https://www.google.com/#q=%@", newText];
        
            request = [NSURLRequest requestWithURL:[NSURL URLWithString:googlestring]];
        }
    }*/
    if (myWebView.isLoading)
    {
        [myWebView stopLoading];
        
    }
    NSURLConnection * conn ;
    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    [myWebView loadRequest:request];

    
}/*
-(void)loadgooglerequest
{
    NSString* newText = (NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)prevString,NULL,(CFStringRef)@"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8);
    
    NSString *googlestring = [NSString stringWithFormat:@"https://www.google.com/?q=%@", newText];
    
    NSURLRequest *request  = [NSURLRequest requestWithURL:[NSURL URLWithString:googlestring]];
    
   // NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:nil];
    

    if (myWebView.isLoading)
    {
        [myWebView stopLoading];
    }
    //[myWebView loadRequest:conn.originalRequest];
    

}*/ 
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // [webView loadHTMLString:[error description] baseURL:nil];
    [progressBar setHidden:YES];
}
/*
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [progressBar setProgress:progressBar.progress + (data.length/self.expectedProgress) ];
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.expectedProgress = response.expectedContentLength;
    //(double) response.progressContentLength / (double) download.expectedContentLength;
}*/

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    [progressBar setHidden:NO];
    [progressBar setProgress:0.3];
    NSString *test = request.URL.absoluteString;
    NSString *pathext = [test pathExtension] ;
    BOOL myval = [pathext isEqualToString:@"mp3"];
    //NSLog(@"r:%@",test);
    //BOOL myval = [test hasPrefix:@"mp3"];
    //NSLog(@"URL REQ : %@", request.URL);
    //[urlField setText:request.URL.absoluteString];
    if (myval)
    {
        //NSLog(@"saatnya download file dari %@", request.URL.absoluteString);
        downloadString = [
                          NSString stringWithString:request.URL.absoluteString] ;
        [self showAutoDownload:nil];
        //NSData *sample = [NSData dataWithContentsOfURL:request.URL];
        
        //[self writeFileWithData:sample Named:filename];
        
    }
    else
    {
        //NSLog(@"jangan di download bukan mp3 ini");
    }
    
    return !myval;

}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.expectedProgress = response.expectedContentLength;
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [progressBar setProgress:progressBar.progress+(data.length/self.expectedProgress)];
}

//NSURLCONNECTION DATA DELEGATE
/*
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response;
{
    
    NSLog(@"will send request : %@", request.URL);
    NSLog(@"will send request with redirect response %@", response.URL);
    return request;
}




- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSLog(@"did receive DATA");
    if ([connection.originalRequest.URL.absoluteString.pathExtension isEqualToString:@"mp3"])
    {
        if(tempData == nil)
        {
            tempData = [NSMutableData dataWithData:data];
        }
        else
        {
            [tempData appendData:data];
        }
    }
}
 */
/*
- (NSInputStream *)connection:(NSURLConnection *)connection needNewBodyStream:(NSURLRequest *)request
{
    return
}
- (void)connection:(NSURLConnection *)connection   didSendBodyData:(NSInteger)bytesWritten
 totalBytesWritten:(NSInteger)totalBytesWritten
totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite
{
    NSLog(@"EVENT DID SEND BODY DATA");
}
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    NSLog(@"CACHED RESPONSE");
    return cachedResponse;
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@" FINISH LOADING ");
}
- (void)connectionDidFinishDownloading:(NSURLConnection *)connection destinationURL:(NSURL *)destinationURL
{
    NSLog(@" FINISH DOWNLOAD %@", [destinationURL absoluteString]);
    if ([connection.originalRequest.URL.absoluteString.pathExtension isEqualToString:@"mp3"])
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        //make a file name to write the data to using the documents directory:
        NSString *fileName = [documentsDirectory stringByAppendingPathComponent:@"test.mp3"];
        
        if ([tempData writeToFile:fileName atomically:YES])
        {
            NSLog(@"WRITE FILE SUCCESS");
        }
        else
            NSLog(@"WRITE FILE FAILED ");
        
    }
}*/

@end
