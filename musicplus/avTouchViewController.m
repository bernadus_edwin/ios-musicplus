/*
 
 File: avTouchViewController.m
 Abstract: View controller class
 Version: 1.3
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2010 Apple Inc. All Rights Reserved.
 
 
 */

#import "avTouchViewController.h"
#import "avTouchController.h"
#import "ReflectionView.h"
#import "AppDelegate.h"

@implementation AssetObject

@synthesize imageArt, albumName, artistName,titleName;

-(id)init
{
    return [super init];
}

-(void)dealloc
{
     
    [super dealloc];
}
@end

@implementation avTouchViewController
@synthesize gallery, playlist, indexPlay, imageArray, assetArray;

// Override initWithNibName:bundle: to load the view using a nib file then perform additional customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * documentDirectory =[[paths objectAtIndex:0] copy];
         self.playlist = [[[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentDirectory error:nil] copy];
        [self generateCahceImage];
        
        
        
        
    }
    return self;
}

-(void ) generateCahceImage
{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentDirectory =[[paths objectAtIndex:0] copy];
    self.imageArray = [[NSMutableArray alloc] init];
    self.assetArray = [[NSMutableArray alloc] init];
    //Isiin semua Image dari Asset ke array
    for (int i =0; i<playlist.count; i++)
    {
        NSURL * fileURL = [[NSURL alloc] initFileURLWithPath:[documentDirectory stringByAppendingPathComponent:[playlist objectAtIndex:i]]];
        
        AVURLAsset *asset = [AVURLAsset URLAssetWithURL: fileURL options:nil];
        
        ///////////// ASSET BEGIN
        NSArray *titles = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata withKey:AVMetadataCommonKeyTitle keySpace:AVMetadataKeySpaceCommon];
        NSArray *artists = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata withKey:AVMetadataCommonKeyArtist keySpace:AVMetadataKeySpaceCommon];
        NSArray *albumNames = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata withKey:AVMetadataCommonKeyAlbumName keySpace:AVMetadataKeySpaceCommon];
        
        //NSLog(@"ASSET = %@", asset);
        AVMetadataItem *title;
        if (titles.count > 0 ) title = [titles objectAtIndex:0];
        else title = [[AVMetadataItem alloc] init];
        
        AVMetadataItem *artist ;
        if (artists.count > 0 ) artist = [artists objectAtIndex:0];
        else artist = [[AVMetadataItem alloc] init];
        
        AVMetadataItem *albumName;
        if (albumNames.count > 0 ) albumName = [albumNames objectAtIndex:0];
        else albumName = [[AVMetadataItem alloc] init];
        
        //NSLog(@"title %@, artist %@, album %@, %@ ---- %@", title.value, artist.value, albumName.value, [playlist objectAtIndex:i], title);
        

        ///////////// ASSET END
        
        
        
        NSArray *keys = [NSArray arrayWithObjects:@"commonMetadata", nil];
        
        void (^testBlock)(void) = ^{
            NSArray *artworks = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata
                                                               withKey:AVMetadataCommonKeyArtwork
                                                              keySpace:AVMetadataKeySpaceCommon];
            
//            if(i == 6) NSLog(@" debug 6 complete");
            for (AVMetadataItem *item in artworks) {
                if ([item.keySpace isEqualToString:AVMetadataKeySpaceID3]) {
                    NSDictionary *d = [item.value copyWithZone:nil];
                    [self.imageArray replaceObjectAtIndex:i withObject: [UIImage imageWithData:[d objectForKey:@"data"]]];
                    //[((AssetObject *)[self.assetArray objectAtIndex:i]) setImageArt:[UIImage imageWithData:[[d objectForKey:@"data"] copy]]];
                    [((AssetObject *)[self.assetArray objectAtIndex:i]) setImageArt:[[UIImage alloc] initWithData:[[d objectForKey:@"data"] copy]]];
                } else if ([item.keySpace isEqualToString:AVMetadataKeySpaceiTunes]) {
                    [self.imageArray replaceObjectAtIndex:i withObject:[UIImage imageWithData:[item.value copyWithZone:nil]]];
                    //((UIImageView *) [gallery itemViewAtIndex:index]).image =[UIImage imageWithData:[item.value copyWithZone:nil]];
                    [((AssetObject *)[self.assetArray objectAtIndex:i]) setImageArt:[UIImage imageWithData:[item.value copyWithZone:nil]]];
                }
            }
        };
        AssetObject * ao = [[AssetObject alloc] init];
        [ao setImageArt:[UIImage imageNamed:@"unknownalbum.png"]];
        if (title.value != nil)
            //[ao setTitleName:[NSString stringWithFormat:@"%@", [title.value copyWithZone:nil]]];
            [ao setTitleName:[[NSString alloc] initWithFormat:@"%@", title.value]];
        else
            [ao setTitleName:[playlist objectAtIndex:i]];
        [ao setAlbumName:[[NSString alloc] initWithFormat:@"%@", albumName.value]];
        [ao setArtistName:[[NSString alloc] initWithFormat:@"%@", artist.value]];
        [assetArray addObject:ao];
        [imageArray addObject:[UIImage imageNamed:@"unknownalbum.png"]];
        [asset loadValuesAsynchronouslyForKeys:keys completionHandler:testBlock];
    }
    
}
-(void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel
{
    //NSLog(@"%@",[playlist objectAtIndex:carousel.currentItemIndex]);
    [self setTitle:[playlist objectAtIndex:carousel.currentItemIndex]];
}
- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    //NSLog(@"index selected %d", index);
    [controller playIndex:index];
    [controller startPlaybackForPlayer:controller.player];

}
-(void)autoPlay
{
    [controller startPlaybackForPlayer:controller.player];
}
- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel;
{
    
    return playlist.count;
}
-(void) getImageFromPlaylistWithIndex:(NSInteger) index
{
    if (index < playlist.count )
    {
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentDirectory =[[paths objectAtIndex:0] copy];
    NSURL * fileURL = [[NSURL alloc] initFileURLWithPath:[documentDirectory stringByAppendingPathComponent:[playlist objectAtIndex:index]]];
     
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL: fileURL options:nil];

     NSArray *keys = [NSArray arrayWithObjects:@"commonMetadata", nil];
    
        void (^testBlock)(void) = ^{
            NSArray *artworks = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata
                                                               withKey:AVMetadataCommonKeyArtwork
                                                              keySpace:AVMetadataKeySpaceCommon];
            if(index == 6 )
            {
                
                //NSLog(@"%@ load complete for index %d , %@",[NSDate date], index ,((UIImageView*)[gallery itemViewAtIndex:index]).image.description);
            }
            for (AVMetadataItem *item in artworks) {
                if ([item.keySpace isEqualToString:AVMetadataKeySpaceID3]) {
                    NSDictionary *d = [item.value copyWithZone:nil];
                    ((UIImageView *) [gallery itemViewAtIndex:index]).image = [UIImage imageWithData:[d objectForKey:@"data"]];
                } else if ([item.keySpace isEqualToString:AVMetadataKeySpaceiTunes]) {
                    
                    ((UIImageView *) [gallery itemViewAtIndex:index]).image =[UIImage imageWithData:[item.value copyWithZone:nil]];
                }
            }
        };
        //NSLog(@"%@ load start for index %d",[NSDate date] , index);
        [asset loadValuesAsynchronouslyForKeys:keys completionHandler:testBlock];
    /*
    [asset loadValuesAsynchronouslyForKeys:keys completionHandler:^{
        NSArray *artworks = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata
                                                           withKey:AVMetadataCommonKeyArtwork
                                                          keySpace:AVMetadataKeySpaceCommon];
        if (index == 6)
            NSLog(@"HARJA %@", artworks);
        for (AVMetadataItem *item in artworks) {
            if ([item.keySpace isEqualToString:AVMetadataKeySpaceID3]) {
                NSDictionary *d = [item.value copyWithZone:nil];
                ((UIImageView *) [gallery itemViewAtIndex:index]).image = [UIImage imageWithData:[d objectForKey:@"data"]];
            } else if ([item.keySpace isEqualToString:AVMetadataKeySpaceiTunes]) {
                
               ((UIImageView *) [gallery itemViewAtIndex:index]).image =[UIImage imageWithData:[item.value copyWithZone:nil]];
            }
        }
    }];
    */
    }
    
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    UIImageView * album = nil;
    //NSLog(@" getViewFor %d", index);
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        //don't do anything specific to the index within
        //this `if (view == nil) {...}` statement because the view will be
        //recycled and used with other index values later
        
        /*view = [[ReflectionView alloc] initWithFrame:CGRectMake(0, 10, 200.0f, 200.0f)];
        [((ReflectionView * )view ) setDynamic:YES];
        [((ReflectionView * )view ) setReflectionScale:0.0f];
        [((ReflectionView * )view ) setReflectionGap:45];
        [((ReflectionView * )view ) setReflectionAlpha:1];
        */
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 60, 200.0f, 200.0f)];
        
        
       // ((UIImageView *)view).image = [UIImage imageNamed:@"unknownalbum.png"];
       // if ([imageArray objectAtIndex:index] != nil)
       //     ((UIImageView *)view).image = [imageArray objectAtIndex:index];
       // view.contentMode = UIViewContentModeScaleAspectFill;
        
        album = [[UIImageView alloc] initWithFrame:CGRectMake(0, 60, 200.0f, 200.0f)];
        album.tag = 2;
        album.contentMode = UIViewContentModeScaleAspectFill;
        [view addSubview:album];
        
        
        //label = [[UILabel alloc] initWithFrame:view.bounds];
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, view.frame.size.width, 22)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        //NSData * greendata = [[NSUserDefaults standardUserDefaults] objectForKey:@"TintColor"];
        //UIColor *greenish = [NSKeyedUnarchiver unarchiveObjectWithData:greendata];
        
        //[label setTextColor:greenish];
        label.font = [label.font fontWithSize:17];
        //[label setMinimumScaleFactor:0.1];
        [label setMinimumScaleFactor:1];
        label.tag = 1;
       // [view addSubview:label];
    }
    else
    {
        album = (UIImageView * ) [view viewWithTag:2];
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    label.text = [NSString stringWithFormat:@"%d", index];
    [label setText:[NSString stringWithFormat:@"%@",((AssetObject * )[assetArray objectAtIndex:index]).titleName]];
    //[album setImage:[imageArray objectAtIndex:index]];
    [album setImage:((AssetObject * ) [assetArray objectAtIndex:index]).imageArt];
    return view;
    
}

// Implement loadView to create a view hierarchy programmatically.
/*- (void)loadView {

	[[avTouchController alloc] init];
}*/
-(NSInteger ) getTableIndexPlay
{
    return self.indexPlay;
}
-(void) setIndexFile:(NSInteger) index
{
    self.indexPlay= index; 
}
- (void)hidePlayer:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}
-(void) setEmptyPlaylist:(BOOL) val
{
    if (controller) controller = [[avTouchController alloc] init];

    isPlaylistEmpty = val;
}
-(void)willMoveToParentViewController:(UIViewController *)parent
{
    [controller setIsPlaylistEmpty:isPlaylistEmpty];
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([playlist count] > 0)
    [self setTitle:[playlist objectAtIndex:indexPlay]];
    else
        [self setTitle:@"Empty"];
}
// Implement viewDidLoad to do additional setup after loading the view.
- (void)viewDidLoad {
   
    
    [super viewDidLoad];
	//[[avTouchController alloc] init]; 
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Hide" style:UIBarButtonItemStyleBordered target:self action:@selector(hidePlayer:)];
    //self.navigationItem.rightBarButtonItem= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:self action:nil];
    NSData * greendata = [[NSUserDefaults standardUserDefaults] objectForKey:@"TintColor"];
    UIColor *greenish = [NSKeyedUnarchiver unarchiveObjectWithData:greendata];
    [self.navigationItem.leftBarButtonItem setTintColor:greenish];
    [self.navigationItem.rightBarButtonItem setTintColor:greenish];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setTintColor:[UIColor grayColor]];
    [self.view setTintColor:greenish];
    [self setTitle:@"Now Playing"];
    [self.gallery setType:iCarouselTypeRotary];
    /*
     _decelerationRate = 0.95f;
     _scrollEnabled = YES;
     _bounces = YES;
     _offsetMultiplier = 1.0f;
     _perspective = -1.0f/500.0f;
     _contentOffset = CGSizeZero;
     _viewpointOffset = CGSizeZero;
     _scrollSpeed = 1.0f;
     _bounceDistance = 1.0f;
     _stopAtItemBoundary = YES;
     _scrollToItemBoundary = YES;
     _ignorePerpendicularSwipes = YES;
     _centerItemWhenSelected = YES;*/
    
    [self.gallery setIgnorePerpendicularSwipes:NO];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bgblue.jpg"]]];


    AppDelegate * sharedApp = (AppDelegate * ) [[UIApplication sharedApplication]delegate];
    if (sharedApp.appAudioPlayer.isPlaying)
    {
        NSInteger gotoIndex =0;
        for(int i = 0 ; i<playlist.count ; i++)
        {
            if ([[sharedApp.appAudioPlayer.url lastPathComponent] isEqualToString:[playlist objectAtIndex:i]])
            {
                gotoIndex = i;
                break;
            }
        }
        [gallery scrollToItemAtIndex:gotoIndex animated:NO];
    }
    else
    {
        [gallery scrollToItemAtIndex:self.indexPlay animated:NO];
    }
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


- (void)dealloc {
    [super dealloc];
}

@end
