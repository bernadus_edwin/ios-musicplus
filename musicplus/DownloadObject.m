//
//  DownloadObject.m
//  musicplus
//
//  Created by Erlangga on 10/9/13.
//  Copyright (c) 2013 Edward Julianto. All rights reserved.
//

#import "DownloadObject.h"

@implementation DownloadObject
@synthesize filename;
@synthesize status;
@synthesize stringURL;
@synthesize stringDate;
- (id)initWith:(NSString *)infilename Status:(NSString *)instatus
{
    self = [super init];
    if (self)
    {
        [self setFilename:infilename];
        [self setStatus:instatus];
    }
    return self;
}
- (id)initWith:(NSString *)strFilename Status:(NSString *)strStatus stringURL:(NSString *)strURL stringDate:(NSString *)strDate
{
    self = [super init];
    if (self)
    {
        [self setFilename:strFilename];
        [self setStatus:strStatus];
        [self setStringURL:strURL];
        [self setStringDate:strDate];
    }
    return self;
}
-(NSDictionary *) getDictionary
{
    NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
    
    [dict setValue:filename forKey:@"filename"];
    [dict setValue:status forKey:@"status"];
    
    [dict setValue:stringURL forKey:@"url"];
    [dict setValue:stringDate forKey:@"date"];
    return (NSDictionary *) dict;
}
@end
