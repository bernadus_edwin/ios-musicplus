//
//  BookmarkTVC.m
//  Music+
//
//  Created by EIT-MACMINI02 on 9/22/13.
//  Copyright (c) 2013 Trinity. All rights reserved.
//

#import "BookmarkTVC.h"
#import "BrowserVC.h"
@interface BookmarkTVC ()

@end

@implementation BookmarkTVC
@synthesize stringURL, arrayBookmarks, parent;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        [self setTitle:@"Bookmarks"];
        [self refreshBookmarkData];
        
    }
    return self;
}
-(void) refreshBookmarkData
{
    arrayBookmarks = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:@"Bookmarks"] copyItems:YES];

    
}
-(void) refreshAll
{
    [self refreshBookmarkData];
    [self.tableView reloadData];
}
-(id) init
{
    return [self initWithStyle:UITableViewStyleGrouped];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"close" style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
    NSData * greendata = [[NSUserDefaults standardUserDefaults] objectForKey:@"TintColor"];
    UIColor *greenish = [NSKeyedUnarchiver unarchiveObjectWithData:greendata];
    [self.navigationItem.rightBarButtonItem setTintColor:greenish];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) addToBookmark
{
    if ( stringURL != nil  && ![stringURL isEqualToString:@""] )
    {
        [arrayBookmarks addObject:stringURL];
    
        [[NSUserDefaults standardUserDefaults] setObject:arrayBookmarks forKey:@"Bookmarks"];
        [self.tableView reloadData];
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Nothing to save" delegate:nil cancelButtonTitle:@"close" otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0 ) return 1;
    
    else if (section == 1 ) return arrayBookmarks.count ;
    else return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    if (indexPath.section == 0 && indexPath.row == 0 )
    {
        [cell.textLabel setText:@"Add Bookmark"];
    }
    if(indexPath.section == 0 && indexPath.row == 1 )
    {
        [cell.textLabel setText:@"Cancel"];
    }
    // Configure the cell...
    if (indexPath.section == 1 && indexPath.row == 0 )
    {
        [cell.textLabel setText:@"Bookmark 1"];
    }
    if(indexPath.section == 1 && indexPath.row == 1)
    {
        [cell.textLabel setText:@"Bookmark 2"];
    }
    if(indexPath.section == 1 && indexPath.row == 2)
    {
        [cell.textLabel setText:@"Bookmark 3"];
    }
    if (indexPath.section == 1 )
    {
        [cell.textLabel setText:[arrayBookmarks objectAtIndex:indexPath.row] ];
    }
    return cell;
}
-(void) dismiss{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    if (indexPath.section == 0 ) return NO;
    else return YES;
}


-(void)deleteBookmark:(NSInteger ) index
{
    [arrayBookmarks removeObjectAtIndex:index];
    [[NSUserDefaults standardUserDefaults] setObject:arrayBookmarks forKey:@"Bookmarks"];
}
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self deleteBookmark:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/
-(NSString * ) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section ==0 ) return @"Action";
    else return @"Saved Bookmarks";
}

#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0 )
    {
        [self addToBookmark];
    }
    if (indexPath.section == 0  && indexPath.row == 1 )
    {
        [self dismiss];
    }
    if (indexPath.section == 1)
    {
        
        [self setStringURL:[arrayBookmarks objectAtIndex:indexPath.row]];
        [self sendDataToParent];
        [self dismiss];
    }
}
-(void) sendDataToParent
{
    if ([self.parent respondsToSelector:@selector(shouldLoadWebRequest:)])
    {
        [parent shouldLoadWebRequest:stringURL];
    }
}
 


@end
