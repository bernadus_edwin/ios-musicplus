//
//  main.m
//  musicplus
//
//  Created by Erlangga on 9/29/13.
//  Copyright (c) 2013 Edward Julianto. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
