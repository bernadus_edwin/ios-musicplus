//
//  AppDelegate.m
//  Music+
//
//  Created by EIT-MACMINI02 on 9/20/13.
//  Copyright (c) 2013 Trinity. All rights reserved.
//
#import "AppDelegate.h"
 
#import "DownloadTVC.h"
#import "BrowserVC.h"
#import "Media.h"
#import "avTouchViewController.h"
#import "avTouchController.h"
#import "iRate.h"
#import "HistoryTVC.h"
@implementation AppDelegate 
-(void) setOldUser
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"OldUser"];
}
-(void) checkPrevUser
{//TOBE ADDED on next (Paid)Version
    //grant access to paid app
}
-(void)addCountOpen
{
   /* NSInteger opentimes = [[NSUserDefaults standardUserDefaults] integerForKey:@"open"];
    if (opentimes < 5 )
    {
        opentimes += 1;
        [[NSUserDefaults standardUserDefaults] setInteger:opentimes forKey:@"open"];
    }
    else
    {
    */
        //TODO tambahin cek rating
        NSString *str = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa";
        str = [NSString stringWithFormat:@"%@/wa/viewContentsUserReviews?", str];
        str = [NSString stringWithFormat:@"%@type=Purple+Software&id=", str];
        
        // Here is the app id from itunesconnect
        str = [NSString stringWithFormat:@"%@717270686", str];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    //}
    
}
+ (void)initialize
{
    //configure iRate
    [[iRate sharedInstance] setDaysUntilPrompt:0];
    
    [[iRate sharedInstance] setUsesUntilPrompt :5];
    //[[iRate sharedInstance] setUsesCount:4];
    //[[iRate sharedInstance] setAppStoreID:717270686];
    [iRate sharedInstance].applicationBundleID = @"com.musicplus";
	//[iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    
    //enable preview mode
    //[iRate sharedInstance].previewMode = YES;

}
-(void) cleanupIndexWeb
{
    NSError * error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"index"];
    if([[NSFileManager defaultManager ] removeItemAtPath:documentsDirectory error:&error])
    {
        NSLog(@"delete Success");
    }
}
-(void) prepareIndexWeb
{
    NSError * error ;

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"index"];
    [[NSFileManager defaultManager ] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:nil];
    
    
    
    NSString * sourcePath = [[NSBundle mainBundle] resourcePath];
    
    NSString * newSourcePath = [sourcePath stringByAppendingPathComponent:@"index.html"];
    NSString * newDocumentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"index.html"];
    [[NSFileManager defaultManager] removeItemAtPath:newDocumentsDirectory error:nil];
    if([[NSFileManager defaultManager] copyItemAtPath:newSourcePath toPath:newDocumentsDirectory error:&error])
    {
        NSLog(@"File successfully copied");
    }else {
        NSLog(@"Error description-%@ \n", [error localizedDescription]);
        NSLog(@"Error reason-%@", [error localizedFailureReason]);
        NSLog(@"from : %@ \nTo : %@", newSourcePath, newDocumentsDirectory);
    }
    
    newSourcePath = [sourcePath stringByAppendingPathComponent:@"pic1.jpg"];
    newDocumentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"pic1.jpg"];
    if([[NSFileManager defaultManager] copyItemAtPath:newSourcePath toPath:newDocumentsDirectory error:&error])
    {
        NSLog(@"File successfully copied");
    }else {
        NSLog(@"Error description-%@ \n", [error localizedDescription]);
        NSLog(@"Error reason-%@", [error localizedFailureReason]);
        NSLog(@"from : %@ \nTo : %@", newSourcePath, newDocumentsDirectory);
    }
    newSourcePath = [sourcePath stringByAppendingPathComponent:@"pic2.jpg"];
    newDocumentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"pic2.jpg"];
    if([[NSFileManager defaultManager] copyItemAtPath:newSourcePath toPath:newDocumentsDirectory error:&error])
    {
        NSLog(@"File successfully copied");
    }else {
        NSLog(@"Error description-%@ \n", [error localizedDescription]);
        NSLog(@"Error reason-%@", [error localizedFailureReason]);
        NSLog(@"from : %@ \nTo : %@", newSourcePath, newDocumentsDirectory);
    }
    newSourcePath = [sourcePath stringByAppendingPathComponent:@"pic3.jpg"];
    newDocumentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"pic3.jpg"];
    if([[NSFileManager defaultManager] copyItemAtPath:newSourcePath toPath:newDocumentsDirectory error:&error])
    {
        NSLog(@"File successfully copied");
    }else {
        NSLog(@"Error description-%@ \n", [error localizedDescription]);
        NSLog(@"Error reason-%@", [error localizedFailureReason]);
        NSLog(@"from : %@ \nTo : %@", newSourcePath, newDocumentsDirectory);
    }
    newSourcePath = [sourcePath stringByAppendingPathComponent:@"pic4.jpg"];
    newDocumentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"pic4.jpg"];
    if([[NSFileManager defaultManager] copyItemAtPath:newSourcePath toPath:newDocumentsDirectory error:&error])
    {
        NSLog(@"File successfully copied");
    }else {
        NSLog(@"Error description-%@ \n", [error localizedDescription]);
        NSLog(@"Error reason-%@", [error localizedFailureReason]);
        NSLog(@"from : %@ \nTo : %@", newSourcePath, newDocumentsDirectory);
    }
    newSourcePath = [sourcePath stringByAppendingPathComponent:@"pic5.jpg"];
    newDocumentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"pic5.jpg"];
    if([[NSFileManager defaultManager] copyItemAtPath:newSourcePath toPath:newDocumentsDirectory error:&error])
    {
        NSLog(@"File successfully copied");
    }else {
        NSLog(@"Error description-%@ \n", [error localizedDescription]);
        NSLog(@"Error reason-%@", [error localizedFailureReason]);
        NSLog(@"from : %@ \nTo : %@", newSourcePath, newDocumentsDirectory);
    }
    newSourcePath = [sourcePath stringByAppendingPathComponent:@"pic6.jpg"];
    newDocumentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"pic6.jpg"];
    if([[NSFileManager defaultManager] copyItemAtPath:newSourcePath toPath:newDocumentsDirectory error:&error])
    {
        NSLog(@"File successfully copied");
    }else {
        NSLog(@"Error description-%@ \n", [error localizedDescription]);
        NSLog(@"Error reason-%@", [error localizedFailureReason]);
        NSLog(@"from : %@ \nTo : %@", newSourcePath, newDocumentsDirectory);
    }
    newSourcePath = [sourcePath stringByAppendingPathComponent:@"pic7.jpg"];
    newDocumentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"pic7.jpg"];
    if([[NSFileManager defaultManager] copyItemAtPath:newSourcePath toPath:newDocumentsDirectory error:&error])
    {
        NSLog(@"File successfully copied");
    }else {
        NSLog(@"Error description-%@ \n", [error localizedDescription]);
        NSLog(@"Error reason-%@", [error localizedFailureReason]);
        NSLog(@"from : %@ \nTo : %@", newSourcePath, newDocumentsDirectory);
    }

    /*
    
    
    NSString *htmlpath = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"];
    [[NSFileManager defaultManager] copyItemAtPath:htmlpath toPath:documentsDirectory error:&copyerror];

    htmlpath = [[NSBundle mainBundle]pathForResource:@"pic1@2x" ofType:@"jpg"];
    [[NSFileManager defaultManager] copyItemAtPath:htmlpath toPath:documentsDirectory error:nil];
    
    [NSFileManager defaultManager]
    
    NSLog(@"%@", [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:&copyerror]);
    NSLog(@"FINISH");
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSError *error  =nil;
    NSString * sourcePath = [[NSBundle mainBundle] resourcePath];*/
    
    
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    //TOBE REMOVED on next (Paid)Version
    [self setOldUser];
    
   // [self prepareIndexWeb];
    [self cleanupIndexWeb];
    //UIColor *  greenish = [UIColor colorWithRed:0.65882354974746704 green:0.89803928136825562 blue:0.41960787773132324 alpha:1 ];
    //UIColor *  greenish =   [UIColor colorWithRed:0.5443753558935136 green:0.80416932397959184 blue:0.31949535366491177 alpha:1 ];
    //FERN
    //UIColor *  greenish = [UIColor colorWithRed:0.25098040699958801 green:0.50196081399917603 blue:0.0 alpha:1 ];
    // <color key="backgroundColor" red="0.25098040699958801" green="0.50196081399917603" blue="0.0" alpha="1"
    //NEW GREEN
    UIColor *  greenish = [UIColor colorWithRed:0.51372551918029785 green:0.73725491762161255 blue:0.29803922772407532 alpha:1 ];
    
    // <color key="backgroundColor" red="0.51372551918029785" green="0.73725491762161255" blue="0.29803922772407532" alpha="1" colorSpace="deviceRGB"/>
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:greenish];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"TintColor"];
     
    [[NSUserDefaults standardUserDefaults ] removeObjectForKey:@"arrayDownload"];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen]bounds]];
    
    
    BrowserVC * browserView = [[BrowserVC alloc] initWithNibName:@"BrowserVC" bundle:[NSBundle mainBundle]];
    DownloadTVC *downloadView = [[DownloadTVC alloc] initWithStyle:UITableViewStylePlain];
    //red="0.65882354974746704" green="0.89803928136825562" blue="0.41960787773132324" alpha="1" colorSpace="deviceRGB"
    //Media * mediaView = [[[Media alloc] initWithStyle:UITableViewStyleGrouped] autorelease];
    self.tabBarController = [[UITabBarController alloc] init];
    
    [self.tabBarController.tabBar setTintColor:greenish];
    
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:browserView];
    [nav.navigationBar setTintColor:greenish];
    [nav.navigationBar setTranslucent:YES];
    UINavigationController * nav2 = [[UINavigationController alloc] initWithRootViewController:downloadView];
    [nav2.navigationBar setTintColor:greenish];
    [nav2.navigationBar setTranslucent:YES];
    [nav2.navigationBar setOpaque:YES];
    /*UINavigationController * nav3 = [[[UINavigationController alloc] initWithRootViewController:mediaView] autorelease];
    [nav3.navigationBar setTintColor:greenish];
    [nav3.navigationBar setTranslucent:NO];
    
    */
    _dManager = [[DownloadSessionManager alloc] init];
   // NSLog(@"main dmanage = %@", _dManager);
    NSString * storyboardName = @"download";
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:[NSBundle mainBundle]];
    
    //UINavigationController * nav4 = (UINavigationController *)[storyboard  instantiateViewControllerWithIdentifier:viewControllerID];
    UINavigationController * nav4 = (UINavigationController *)[storyboard instantiateInitialViewController];

    HistoryTVC * hist = [[HistoryTVC alloc] initWithStyle:UITableViewStylePlain];
    UINavigationController * nav5= [[UINavigationController alloc] initWithRootViewController:hist];

    
    [self.tabBarController.tabBar setTranslucent:NO];
    //[self.tabBarController.tabBar setBarStyle:UIBarStyleBlack];
    [nav.navigationBar setBarStyle:UIBarStyleBlack];
        [nav4.navigationBar setBarStyle:UIBarStyleBlack];
        [nav5.navigationBar setBarStyle:UIBarStyleBlack];
        [nav2.navigationBar setBarStyle:UIBarStyleBlack];
    UIColor * tungsten = [UIColor colorWithRed:0.20000000298023224 green:0.20000000298023224 blue:0.20000000298023224 alpha:1];
    //<color key="backgroundColor" red="0.20000000298023224" green="0.20000000298023224" blue="0.20000000298023224" alpha="1" colorSpace="calibratedRGB"/>
    [nav.navigationBar setBarTintColor:tungsten];
    [nav4.navigationBar setBarTintColor:tungsten];
    [nav5.navigationBar setBarTintColor:tungsten];
    [nav2.navigationBar setBarTintColor:tungsten];
    
    [self.tabBarController.tabBar setBarTintColor:tungsten];
    [self.tabBarController setViewControllers:@[nav, nav4, nav5 ,nav2 ]];
    
    self.window.rootViewController = self.tabBarController;
 
    [self moveSomething];
    [self.window makeKeyAndVisible];
    //[self excludeBackupForAllMusicFiles];
    [self excludeDocumentFolder];
    return YES;
    
}
-(BOOL) excludeDocumentFolder
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSURL *directoryURL = [[NSURL alloc] initFileURLWithPath:documentsDirectory isDirectory:YES];
    NSLog(@"%@", directoryURL);
    
    NSError *error = nil;
    BOOL success = [directoryURL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding FOLDER %@ from backup %@", [directoryURL lastPathComponent], error);
    }
    return success;
}

-(void) excludeBackupForAllMusicFiles
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *destpath = @"";
    
    
    NSMutableArray * directoryContent = [[NSMutableArray alloc ] initWithArray:[[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:nil]  copyItems:YES];
    
    //foreach object in directory  exclude it from backup
    for (NSString * filename in directoryContent) {
        destpath = [documentsDirectory stringByAppendingPathComponent:filename];
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:destpath]];
    }
    //{
    // get filename
    // destpath =[documentsDirectory stringByAppendingPathComponent:filename];
    //
    
    
    // [self addSkipBackupAttributeToItemAtURL:url from filename]
    //}
}
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

-(void) moveSomething
{
    /*
     NSArray *paths = NSSearchPathForDirectoriesInDomains
     (NSDocumentDirectory, NSUserDomainMask, YES);
     NSString *documentsDirectory = [paths objectAtIndex:0];
     NSError *error  =nil;
     NSString * sourcePath = [[NSBundle mainBundle] resourcePath];
     
     NSString * newSourcePath = [sourcePath stringByAppendingPathComponent:@"preview.mp3"];
     NSString * newDocumentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"preview.mp3"];
     if([[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:newDocumentsDirectory error:&error])
     {
     NSLog(@"File successfully copied");
     }else {
     NSLog(@"Error description-%@ \n", [error localizedDescription]);
     NSLog(@"Error reason-%@", [error localizedFailureReason]);
     NSLog(@"from : %@ \nTo : %@", newSourcePath, newDocumentsDirectory);
     }
     newSourcePath = [sourcePath stringByAppendingPathComponent:@"Roar_Lions_Roar.mp3"];
     newDocumentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"Roar_Lions_Roar.mp3"];
     if([[NSFileManager defaultManager] copyItemAtPath:newSourcePath toPath:newDocumentsDirectory error:&error])
     {
     NSLog(@"File successfully copied");
     }else {
     NSLog(@"Error description-%@ \n", [error localizedDescription]);
     NSLog(@"Error reason-%@", [error localizedFailureReason]);
     NSLog(@"from : %@ \nTo : %@", newSourcePath, newDocumentsDirectory);
     }
     
     newSourcePath = [sourcePath stringByAppendingPathComponent:@"test.mp3"];
     newDocumentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"test.mp3"];
     if([[NSFileManager defaultManager] copyItemAtPath:newSourcePath toPath:newDocumentsDirectory error:&error])
     {
     NSLog(@"File successfully copied");
     }else {
     NSLog(@"Error description-%@ \n", [error localizedDescription]);
     NSLog(@"Error reason-%@", [error localizedFailureReason]);
     NSLog(@"from : %@ \nTo : %@", newSourcePath, newDocumentsDirectory);
     }
     newSourcePath = [sourcePath stringByAppendingPathComponent:@"Miley Cyrus - Wrecking Ball [128].mp3"];
     newDocumentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"Miley Cyrus - Wrecking Ball [128].mp3"];
     if([[NSFileManager defaultManager] copyItemAtPath:newSourcePath toPath:newDocumentsDirectory error:&error])
     {
     NSLog(@"File successfully copied");
     }else {
     NSLog(@"Error description-%@ \n", [error localizedDescription]);
     NSLog(@"Error reason-%@", [error localizedFailureReason]);
     NSLog(@"from : %@ \nTo : %@", newSourcePath, newDocumentsDirectory);
     }
     newSourcePath = [sourcePath stringByAppendingPathComponent:@"Grow A Pear [WikiSeda].mp3"];
     newDocumentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"Grow A Pear [WikiSeda].mp3"];
     if([[NSFileManager defaultManager] copyItemAtPath:newSourcePath toPath:newDocumentsDirectory error:&error])
     {
     NSLog(@"File successfully copied");
     }else {
     NSLog(@"Error description-%@ \n", [error localizedDescription]);
     NSLog(@"Error reason-%@", [error localizedFailureReason]);
     NSLog(@"from : %@ \nTo : %@", newSourcePath, newDocumentsDirectory);
     }*/
    
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/*
 // Optional UITabBarControllerDelegate method.
 - (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
 {
 }
 */

/*
 // Optional UITabBarControllerDelegate method.
 - (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
 {
 }
 */

#pragma mark Background completion handler
- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier
  completionHandler:(void (^)())completionHandler
{ 
    /*
     Store the completion handler. The completion handler is invoked by the view controller's checkForAllDownloadsHavingCompleted method (if all the download tasks have been completed).
     */
	self.backgroundSessionCompletionHandler = completionHandler;
}


@end
